package ru.maslov.ejuicecalendar.event_bus;

/**
 * Created by Администратор on 18.08.2016.
 */
public abstract class RefreshEvent {
    public int type;

    public static final int TYPE_REFRESH = 100;

    public RefreshEvent(int type) {
        this.type = type;
    }
}
