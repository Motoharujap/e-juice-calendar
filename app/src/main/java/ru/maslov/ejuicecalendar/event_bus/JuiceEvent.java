package ru.maslov.ejuicecalendar.event_bus;

import android.support.annotation.Nullable;

/**
 * Created by Администратор on 31.07.2016.
 */
public class JuiceEvent extends RefreshEvent{
    public static final int EVENT_DELETE = 10;

    private String mEventMsg;

    public JuiceEvent(int mEventType, @Nullable String mEventMsg) {
        super(mEventType);
        this.mEventMsg = mEventMsg;
    }

    public String getEventMsg() {
        return mEventMsg != null ? mEventMsg : "";
    }
}
