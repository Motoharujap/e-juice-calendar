package ru.maslov.ejuicecalendar.event_bus;

/**
 * Created by Администратор on 18.08.2016.
 */
public class RecipeEvent extends RefreshEvent{
    public static final int RECIPE_LOADING_FAILED_NO_CONNECTION = 404;

    public RecipeEvent(int eventType) {
        super(eventType);
    }
}
