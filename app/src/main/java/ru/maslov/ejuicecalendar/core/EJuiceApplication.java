package ru.maslov.ejuicecalendar.core;

import android.app.Application;
import android.os.Environment;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.io.IOException;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.database.JuiceDatabase;
import ru.maslov.ejuicecalendar.tools.FileTools;

/**
 * Created by Администратор on 23.07.2016.
 */
public class EJuiceApplication extends Application {
    private static EJuiceApplication mInstance;
    private String databaseInternalPath;
    private String databaseSdCardPath;
    public static final String DATABASE_FILE_NAME = JuiceDatabase.NAME + ".db";

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        FlowManager.init(new FlowConfig.Builder(this).build());
        databaseSdCardPath = Environment.getExternalStorageDirectory().toString() + "/" + DATABASE_FILE_NAME;
        databaseInternalPath = super.getDatabasePath(DATABASE_FILE_NAME).getPath();
    }
    public static EJuiceApplication getInstance(){
        return mInstance;
    }
    public String getDatabaseInternalPath() {
        return databaseInternalPath;
    }

    public void copyDBToSDCard() throws IOException {
        FileTools.copy(databaseInternalPath, databaseSdCardPath);
    }

    public void copyDb(int commandId) throws IOException {
        switch (commandId){
            case R.id.action_db_to_sd:{
                copyDBToSDCard();
                break;
            }
            case R.id.action_db_to_app:{
                copyDBFromSDCard();
                break;
            }
        }
    }

    public void copyDBFromSDCard() throws IOException {
        FileTools.checkIfDatabaseFolderExists();
        FileTools.checkIfDatabaseFileExists();
        FileTools.copy(databaseSdCardPath, databaseInternalPath);
    }

}
