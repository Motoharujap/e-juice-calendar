package ru.maslov.ejuicecalendar.core;

import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.ui.CreateEJuiceActivity;
import ru.maslov.ejuicecalendar.ui.CreateRecipeActivity;
import ru.maslov.ejuicecalendar.ui.CreateRecipeFragment;
import ru.maslov.ejuicecalendar.ui.FlavorListActivity;
import ru.maslov.ejuicecalendar.ui.RecipeListActivity;
import ru.maslov.ejuicecalendar.ui.StartActivity;
import ru.maslov.ejuicecalendar.ui.presenters.CreateRecipeFragmentPresenter;
import ru.maslov.sandbox.BaseActivity;

/**
 * Created by Администратор on 24.07.2016.
 */
public abstract class CoreActivity extends BaseActivity {
    public static final String DIALOG = "dialog";
    @Override
    protected int getToolbarResId() {
        return R.id.toolbar;
    }

    @Override
    protected int getDrawerResId() {
        return R.id.drawer_layout;
    }

    @Override
    protected int getNavigationViewResId() {
        return R.id.nav_view;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.create_ejuice:{
                gotoActivity(CoreActivity.this, CreateEJuiceActivity.class);
                break;
            }
            case R.id.home:{
                gotoActivity(CoreActivity.this, StartActivity.class);
                break;
            }
            case R.id.create_recipe_menu:{
                Bundle args = new Bundle();
                args.putInt(CreateRecipeFragment.KEY_EDITING_MODE, CreateRecipeFragmentPresenter.EditMode.MODE_CREATE_RECIPE_ALL_CATEGORIES);
                gotoActivity(CoreActivity.this, CreateRecipeActivity.class);
                break;
            }
            case R.id.choose_recipe_menu:{
                gotoActivity(CoreActivity.this, RecipeListActivity.class);
                break;
            }
            case R.id.flavorList:{
                gotoActivity(CoreActivity.this, FlavorListActivity.class);
                break;
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
