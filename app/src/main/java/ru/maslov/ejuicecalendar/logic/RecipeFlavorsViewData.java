package ru.maslov.ejuicecalendar.logic;

import ru.maslov.ejuicecalendar.database.IDatabaseModelRepresentation;
import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors;

/**
 * Created by Администратор on 02.10.2016.
 */

public class RecipeFlavorsViewData implements IDatabaseModelRepresentation {
    private Flavor flavor;
    private RecipeFlavors recipeFlavors;

    public RecipeFlavorsViewData() {
        recipeFlavors = new RecipeFlavors();
    }

    public RecipeFlavors getRecipeFlavors() {
        return recipeFlavors;
    }

    public void setRecipeFlavors(RecipeFlavors recipeFlavors) {
        this.recipeFlavors = recipeFlavors;
    }

    public Flavor getFlavor() {
        if (flavor == null){
            return recipeFlavors.getFlavor();
        }
        return flavor;
    }

    public void setFlavor(Flavor flavor) {
        this.flavor = flavor;
    }

    @Override
    public void update() {
        if (flavor != null) {
            flavor.save();
            recipeFlavors.setFlavorId(flavor.getId());
        }
        recipeFlavors.update();
    }

    @Override
    public void delete() {
        recipeFlavors.delete();
    }

    @Override
    public void save() {
        if (flavor != null){
            flavor.save();
            recipeFlavors.setFlavorId(flavor.getId());
        }
        recipeFlavors.save();
    }
}
