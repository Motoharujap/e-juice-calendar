package ru.maslov.ejuicecalendar.logic;

/**
 * In CreateRecipeFragment we generate flavor views dynamically,
 * so we need to save added views data so it can survive configuration change.
 * Based on the information we save to this class, we can restore views and fill them with info
 * after configuration change
 */
public class FlavorViewData {
    public String flavorName = "Аромка";
    public double flavorVolumePercents = 0.0;
    public double flavorVolumeMilliliters = 0.0;

    //used in case if flavor was chosen, not created from scratch
    public long flavorId;

    public FlavorViewData() {
    }

    public FlavorViewData(String flavorName, double flavorVolumePercents) {
        this.flavorName = flavorName;
        this.flavorVolumePercents = flavorVolumePercents;
    }

}
