package ru.maslov.ejuicecalendar.logic;

import java.io.Serializable;

/**
 * Created by Администратор on 29.08.2016.
 */
public class JuiceParams implements Serializable{
    public long recipeId;
    public double containerVolume;
    public double pg;
    public double vg;

    public JuiceParams() {
    }

    public JuiceParams(long recipeId, double containerVolume, double pg, double vg) {
        this.recipeId = recipeId;
        this.containerVolume = containerVolume;
        this.pg = pg;
        this.vg = vg;
    }
}
