package ru.maslov.ejuicecalendar.ui.presenters;

import com.raizlabs.android.dbflow.sql.language.Condition;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.data.Query;
import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.database.model.Flavor_Table;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.ICreateFlavorView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 09.09.2016.
 */
public class CreateFlavorPresenter extends BasePresenter<ICreateFlavorView> {

    public void saveFlavor(String flavorName, boolean isInStock){
        if (JuiceDataProvider.getFlavor(new Query(Condition.column(Flavor_Table.name.getNameAlias()).is(flavorName))).equals(Flavor.NULL)) {
            Flavor flavor = new Flavor();
            flavor.setName(flavorName);
            flavor.setIsInStock(isInStock ? 1 : 0);
            flavor.save();
        }
    }

    public void editFlavor(String flavorOriginalName, String flavorChangedName){
        Flavor flavor;
        if (!(flavor = JuiceDataProvider.getFlavor(new Query(Condition.column(Flavor_Table.name.getNameAlias()).is(flavorOriginalName)))).equals(Flavor.NULL)) {
            flavor.setName(flavorChangedName);
            flavor.update();
        }
    }

    @Override
    protected Class getDataManagerClass() {
        return null;
    }
}
