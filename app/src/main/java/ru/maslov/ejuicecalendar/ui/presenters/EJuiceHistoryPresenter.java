package ru.maslov.ejuicecalendar.ui.presenters;

import ru.maslov.ejuicecalendar.ui.dataManagers.EJuiceHistoryDataManager;
import ru.maslov.ejuicecalendar.ui.dataManagers.EjuiceScheduleDataManager;

/**
 * Created by Администратор on 02.09.2016.
 */
public class EJuiceHistoryPresenter extends EjuiceSchedulePresenter {

    @Override
    protected Class getDataManagerClass() {
        return EJuiceHistoryDataManager.class;
    }

    @Override
    protected EjuiceScheduleDataManager getDataManager() {
        return (EJuiceHistoryDataManager) mDataManager;
    }
}
