package ru.maslov.ejuicecalendar.ui.presenters;

import java.util.Calendar;
import java.util.Date;

import ru.maslov.ejuicecalendar.alarms.AlarmScheduler;
import ru.maslov.ejuicecalendar.core.EJuiceApplication;
import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.tools.DateTools;
import ru.maslov.ejuicecalendar.ui.dataManagers.EJuiceCreatorDataManager;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IEJuiceCreatorView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 23.07.2016.
 */
public class EJuiceCreatorPresenter extends BasePresenter<IEJuiceCreatorView> {
    public Recipe recipe;
    public void saveJuiceReadyDate(Date readyDate){
        getDataManager().getJuice().setReadyDate(readyDate);
    }

    public void saveJuiceContainerVolume(double volume){
        getDataManager().getJuice().setContainerVolume(volume);
        getDataManager().getJuice().update();
    }

    public void initJuiceByRecipeId(long recipeId){
        recipe = getDataManager().getRecipeById(recipeId);
        if (recipe != null){
            getDataManager().getJuice().setRecipeId(recipeId);
            getDataManager().getJuice().setReadyDate(getRecipeReadyDate());
            view().fillViewWithJuiceData(getDataManager().getJuice());
        }
    }

    public void initJuiceByJuiceId(long juiceId){
        Juice juice = getDataManager().createJuice(juiceId);
        if (!juice.equals(Juice.NULL)){
            view().fillViewWithJuiceData(getDataManager().getJuice());
        }
    }


    public String getRecipeReadyDateString(){
        return DateTools.dateToString(getRecipeReadyDate(), DateTools.JUST_DATE);
    }

    public Date getRecipeReadyDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + recipe.getDecoctionPeriodLength());
        return calendar.getTime();
    }

    //should only be used when recipe is not null
    public Calendar calculateJuiceReadyDate(){
        int recipeDecoctionPeriod = recipe.getDecoctionPeriodLength();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + recipeDecoctionPeriod);
        return calendar;
    }

    public void saveJuiceName(String name){
        getDataManager().getJuice().setJuiceName(name);
        getDataManager().getJuice().update();
    }

    public void savePGValue(double pg){
        getDataManager().getJuice().setPgVolume(pg);
        getDataManager().getJuice().update();
    }

    public void saveVGValue(double vg){
        getDataManager().getJuice().setVgVolume(vg);
        getDataManager().getJuice().update();
    }

    public void saveJuice(){
        getDataManager().saveJuice();
        AlarmScheduler.scheduleJuiceReadyNotification(EJuiceApplication.getInstance(), getJuice());
    }

    public Juice getJuice(){
        return getDataManager().getJuice();
    }

    @Override
    protected Class getDataManagerClass() {
        return EJuiceCreatorDataManager.class;
    }

    private EJuiceCreatorDataManager getDataManager() {
        return (EJuiceCreatorDataManager) mDataManager;
    }

    protected IEJuiceCreatorView view(){
        if (mView.get() == null){
            return new IEJuiceCreatorView() {
                @Override
                public void fillViewWithJuiceData(Juice juice) {

                }

                @Override
                public String getString(int i) {
                    return "";
                }
            };
        }
        return mView.get();
    }
}
