package ru.maslov.ejuicecalendar.ui.dataManagers;

import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.database.model.Juice_Table;
import ru.maslov.ejuicecalendar.event_bus.JuiceEvent;
import ru.maslov.ejuicecalendar.event_bus.ScheduleEvent;

/**
 * Created by Администратор on 25.07.2016.
 */
public class EjuiceScheduleDataManager extends BaseJuiceDataManager {

    public EjuiceScheduleDataManager() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public ArrayList<Juice> startDataLoading(){
        if (mIsReloadingRequired) {
            mJuices = (ArrayList<Juice>) new Select().from(Juice.class).where(Condition.column(Juice_Table.isFinished.getNameAlias()).is(0)).queryList();
            mIsReloadingRequired = false;
        }
        return mJuices;
    }

    @Subscribe
    public void onJuiceEvent(JuiceEvent event){
        switch(event.type){
            case JuiceEvent.TYPE_REFRESH:{
                setDataChanged();
                startDataLoading();
                EventBus.getDefault().post(new ScheduleEvent(ScheduleEvent.TYPE_REFRESH));
            }
        }
    }
}
