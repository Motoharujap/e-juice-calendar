package ru.maslov.ejuicecalendar.ui.list.adapters;

import android.view.MenuItem;

/**
 * Created by Администратор on 26.07.2016.
 */
public interface OnMenuItemClickListener {
    void onMenuItemSelected(MenuItem item, int itemPosition);
}
