package ru.maslov.ejuicecalendar.ui.presenters;

import ru.maslov.ejuicecalendar.ui.viewinterfaces.IStartActivityView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 23.07.2016.
 */
public class StartActivityPresenter extends BasePresenter<IStartActivityView> {
    @Override
    protected Class getDataManagerClass() {
        return null;
    }
}
