package ru.maslov.ejuicecalendar.ui.list.listItems;

/**
 * Created by Администратор on 17.08.2016.
 */
public class RecipeFlavorItem {
    public String flavorName;
    public double flavorVolume;

    public RecipeFlavorItem(String flavorName, double flavorVolume) {
        this.flavorName = flavorName;
        this.flavorVolume = flavorVolume;
    }
}
