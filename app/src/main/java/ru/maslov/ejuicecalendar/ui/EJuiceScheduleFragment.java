package ru.maslov.ejuicecalendar.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.Bind;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.event_bus.ScheduleEvent;
import ru.maslov.ejuicecalendar.ui.dialogs.DialogManager;
import ru.maslov.ejuicecalendar.ui.list.adapters.OnItemClickListener;
import ru.maslov.ejuicecalendar.ui.list.adapters.OnMenuItemClickListener;
import ru.maslov.ejuicecalendar.ui.list.adapters.ScheduleAdapter;
import ru.maslov.ejuicecalendar.ui.list.listItems.ScheduleItem;
import ru.maslov.ejuicecalendar.ui.presenters.EjuiceSchedulePresenter;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IEjuiceScheduleView;
import ru.maslov.sandbox.BaseFragment;

/**
 * Created by Администратор on 23.07.2016.
 */
public class EJuiceScheduleFragment extends BaseFragment<EjuiceSchedulePresenter> implements IEjuiceScheduleView{
    @Bind(R.id.scheduleGridView)protected GridView mScheduleGridView;
    @Bind(R.id.onGridViewEmpty) protected TextView mGridViewEmptyMessage;
    @Bind(R.id.fab) protected FloatingActionButton mFab;
    private ScheduleAdapter mAdapter;

    public static EJuiceScheduleFragment getInstance(){
        return new EJuiceScheduleFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = super.onCreateView(inflater, container, savedInstanceState);
        mAdapter = (ScheduleAdapter) getGridAdapter();
        mAdapter.setOnMenuItemSelectedListener(new OnMenuItemClickListener() {
            @Override
            public void onMenuItemSelected(MenuItem item, int itemPosition) {
                mPresenter.deleteJuice(itemPosition);
            }
        });
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                long juiceId = mPresenter.getJuiceIdByPosition(position);
                showJuiceInfoDialog(juiceId);
            }
        });

        mScheduleGridView.setAdapter(mAdapter);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoActivity(getActivity(), CreateEJuiceActivity.class);
            }
        });
        mFab.setVisibility(isFABVisible() ? View.VISIBLE : View.GONE);

        EventBus.getDefault().register(this);
        return mainView;
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    protected String getEmptyListMessage(){
        return getString(R.string.ejuice_schedule_empty);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();
    }

    @Subscribe
    public void onScheduleEvent(ScheduleEvent event){
        switch(event.type){
            case ScheduleEvent.TYPE_REFRESH:{
                mPresenter.loadData();
            }
        }
    }

    protected boolean isFABVisible(){
        return true;
    }

    protected BaseAdapter getGridAdapter(){
        return new ScheduleAdapter(getActivity());
    }

    protected void showJuiceInfoDialog(long juiceId){
        if (juiceId != -1){
            DialogManager.juiceInfoDialog(juiceId).show(getFragmentManager(), "dialog");
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.ejuice_schedule_fragment;
    }

    @Override
    protected EjuiceSchedulePresenter getPresenter() {
        if (mPresenter == null){
            mPresenter = new EjuiceSchedulePresenter();
        }
        return mPresenter;
    }


    @Override
    public void onItemsLoaded(ArrayList<ScheduleItem> items) {
        if (!items.isEmpty()) {
            mScheduleGridView.setVisibility(View.VISIBLE);
            mGridViewEmptyMessage.setVisibility(View.GONE);
        } else {
            mGridViewEmptyMessage.setText(getEmptyListMessage());
            mScheduleGridView.setVisibility(View.GONE);
            mGridViewEmptyMessage.setVisibility(View.VISIBLE);
        }
        mAdapter.setItems(items);
        mAdapter.notifyDataSetChanged();
    }

}
