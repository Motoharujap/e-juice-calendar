package ru.maslov.ejuicecalendar.ui.list.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.ui.list.listItems.ScheduleItem;

/**
 * Created by Администратор on 23.07.2016.
 */
public class ScheduleAdapter extends BaseAdapter {
    private ArrayList<ScheduleItem> mItems = new ArrayList<>();
    protected Context mContext;
    private OnItemClickListener mOnItemClickListener;
    private OnMenuItemClickListener mOnMenuItemClickListener;

    public ScheduleAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setOnMenuItemSelectedListener(OnMenuItemClickListener listener){
        mOnMenuItemClickListener = listener;
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mOnItemClickListener = listener;
    }

    public void setItems(ArrayList<ScheduleItem> items){
        mItems = items;
    }
    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.schedule_list_item, null);
            holder = new ViewHolder();
            holder.juiceName = (TextView) convertView.findViewById(R.id.juiceName);
            holder.readyIn = (TextView) convertView.findViewById(R.id.timeLeftTillReady);
            holder.overflowMenu = (ImageButton) convertView.findViewById(R.id.overflowMenu);
            holder.background = (LinearLayout) convertView.findViewById(R.id.background);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ScheduleItem item = (ScheduleItem) getItem(position);
        holder.juiceName.setText(item.getJuiceName());
        holder.readyIn.setText(item.getReadyIn());
        holder.overflowMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(mContext, v);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        mOnMenuItemClickListener.onMenuItemSelected(item, position);
                        return true;
                    }
                });
                menu.inflate(R.menu.grid_context_menu);
                menu.show();
            }
        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, position);
            }
        });
        holder.background.setBackgroundColor(getItemBackgroundColor());
        return convertView;
    }

    protected int getItemBackgroundColor(){
        return mContext.getResources().getColor(R.color.material_red_400);
    }

    class ViewHolder {
        TextView juiceName;
        TextView readyIn;
        ImageButton overflowMenu;
        LinearLayout background;
    }
}
