package ru.maslov.ejuicecalendar.ui.dialogs;

import android.os.Bundle;

import ru.maslov.ejuicecalendar.R;

/**
 * Created by Администратор on 02.09.2016.
 */
public class JuiceHistoryInfoDialog extends JuiceInfoDialog {

    public static JuiceHistoryInfoDialog getInstance(Bundle args){
        JuiceHistoryInfoDialog dialog = new JuiceHistoryInfoDialog();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    protected boolean isRatingBarEnabled() {
        return true;
    }

    @Override
    protected boolean isDaysLeftContainerVisible() {
        return false;
    }

    @Override
    protected String ratingNotDefinedCaption() {
        return getString(R.string.rate_recipe_pls);
    }
}
