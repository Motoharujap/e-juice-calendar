package ru.maslov.ejuicecalendar.ui.presenters;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors;
import ru.maslov.ejuicecalendar.logic.FlavorViewData;
import ru.maslov.ejuicecalendar.logic.JuiceParams;
import ru.maslov.ejuicecalendar.ui.IFlavorViewDataProvider;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IJuiceFlavorVolumeView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 20.08.2016.
 */
public class JuiceFlavorVolumeDialogPresenter extends BasePresenter<IJuiceFlavorVolumeView> implements IFlavorViewDataProvider{
    public Recipe recipe;

    public ArrayList<RecipeFlavors> recipeFlavors = new ArrayList<>();

    public void initJuice(long recipeId){
        recipe = JuiceDataProvider.getRecipeById(recipeId);
        recipeFlavors = JuiceDataProvider.getRecipeFlavorsForRecipe(recipeId);
    }

    @Override
    public ArrayList<FlavorViewData> getFlavorViewData(JuiceParams params){
        return JuiceDataProvider.getFlavorViewData(params);
    }

    @Override
    protected Class getDataManagerClass() {
        return null;
    }
}
