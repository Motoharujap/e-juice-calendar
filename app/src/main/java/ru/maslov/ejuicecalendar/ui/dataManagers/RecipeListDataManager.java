package ru.maslov.ejuicecalendar.ui.dataManagers;

import com.raizlabs.android.dbflow.sql.language.Condition;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.data.RecipeInfo;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.event_bus.RecipeEvent;
import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;

/**
 * Created by Администратор on 17.08.2016.
 */
public class RecipeListDataManager extends BaseListDataManager<RecipeInfo> {

    @Override
    protected ArrayList<RecipeInfo> loadData(Condition.In condition) {
        ArrayList<Recipe> recipes = JuiceDataProvider.getRecipes(condition);
        ArrayList<RecipeInfo> recipeInfos = new ArrayList<>();
        for (Recipe recipe : recipes){
            RecipeInfo info = new RecipeInfo(recipe);
            recipeInfos.add(info);
        }
        return recipeInfos;
    }

    public ArrayList<RecipeItem> getRecipeItems(Condition.In condition){
        ArrayList<RecipeInfo> recipes = getObjects(condition);
        ArrayList<RecipeItem> recipeItems = new ArrayList<>();
        for (RecipeInfo recipeInfo : recipes){
            RecipeItem item = new RecipeItem(recipeInfo.recipe, recipeInfo.recipeFlavors);
            recipeItems.add(item);
        }
        return recipeItems;
    }

    @Subscribe
    public void onRecipeEvent(RecipeEvent event){
        switch (event.type){
            case RecipeEvent.TYPE_REFRESH:{
                setDataChanged();
            }
        }
    }
}
