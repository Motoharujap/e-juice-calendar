package ru.maslov.ejuicecalendar.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jakewharton.rxbinding.support.v7.widget.RxSearchView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.Bind;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.event_bus.DataManagerReloadEvent;
import ru.maslov.ejuicecalendar.event_bus.RecipeEvent;
import ru.maslov.ejuicecalendar.network.rss.connectivity.ConnectionMonitor;
import ru.maslov.ejuicecalendar.ui.dialogs.DialogManager;
import ru.maslov.ejuicecalendar.ui.list.adapters.OnMenuItemClickListener;
import ru.maslov.ejuicecalendar.ui.list.adapters.OnRecipeChosenClickListener;
import ru.maslov.ejuicecalendar.ui.list.adapters.RecipeListAdapter;
import ru.maslov.ejuicecalendar.ui.list.decorations.DividerItemDecoration;
import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;
import ru.maslov.ejuicecalendar.ui.presenters.RecipeListPresenter;
import ru.maslov.ejuicecalendar.ui.stateMachine.InStockButtonFilterState;
import ru.maslov.ejuicecalendar.ui.stateMachine.OnStateChangedListener;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IRecipeListFragmentView;
import ru.maslov.sandbox.BaseFragment;
import ru.maslov.sandbox.dialogs.Dialog;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by Администратор on 17.08.2016.
 */
public class RecipeListFragment extends BaseFragment<RecipeListPresenter> implements IRecipeListFragmentView{
    @Bind(R.id.list)protected RecyclerView recipesList;
    @Bind(R.id.fab)protected FloatingActionButton fab;
    @Bind(R.id.coordinator)protected CoordinatorLayout coordinatorLayout;
    @Bind(R.id.inStockFilterBtn)protected Button inStockFilterBtn;
    private RecipeListAdapter adapter;
    private ProgressDialog progressDialog;

    public static final String KEY_RECIPE_CATEGORY = "recipe_category";

    public static RecipeListFragment getInstance(@Nullable Bundle args){
        RecipeListFragment recipeListFragment = new RecipeListFragment();
        if (args != null){
            recipeListFragment.setArguments(args);
        }
        return recipeListFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        initFilterBtnStates();
        parseArguments();
        inStockFilterBtn.setText(getString(mPresenter.currentState().getBtnTextResId()));
        mPresenter.setStateChangedListener(new OnStateChangedListener<InStockButtonFilterState>() {
            @Override
            public void onStateChanged(InStockButtonFilterState newState) {
                inStockFilterBtn.setText(getString(newState.getBtnTextResId()));
            }
        });
        adapter = new RecipeListAdapter(getActivity());
        adapter.setOnRecipeCreateListener(new OnRecipeChosenClickListener() {
            @Override
            public void onRecipeChosen(final int position) {
                Bundle args = new Bundle();
                args.putLong(EJuiceCreatorFragment.KEY_RECIPE_ID, mPresenter.getRecipeInfoByListPosition(position).recipe.getId());
                gotoActivity(getActivity(), CreateEJuiceActivity.class, args);
            }
        });
        adapter.setOnRecipeDeleteListener(new OnRecipeChosenClickListener() {
            @Override
            public void onRecipeChosen(final int position) {
                DialogManager.confirmDialog(null, getString(R.string.delete_recipe_confirmation), Dialog.DIALOG_CONFIRM, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mPresenter.deleteRecipe(position);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show(getFragmentManager(), "dialog");

            }
        });
        recipesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        recipesList.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        recipesList.setAdapter(adapter);

        inStockFilterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.stateMachine().currentState().getClickListener().onClick(v);
            }
        });
        adapter.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public void onMenuItemSelected(MenuItem item, int itemPosition) {
                switch (item.getItemId()){
                    case R.id.deleteRecipe:{
                        mPresenter.deleteRecipe(itemPosition);
                        break;
                    }
                    case R.id.editRecipe:{
                        mPresenter.editRecipe(itemPosition);
                        break;
                    }
                }
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.onFabClicked();
            }
        });
        return mainView;
    }

    @Override
    public void onDestroyView() {
        mPresenter.saveCurrentFilterBtnState(mPresenter.stateMachine().currentState());
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.recipe_list_activity_menu, menu);
        final SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        initSearchViewTextListener(searchView);
        searchView.setIconifiedByDefault(false);
        MenuItemCompat.OnActionExpandListener expandListener = new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                searchView.requestFocus();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                mPresenter.loadData();
                return true;
            }
        };
        MenuItemCompat.setOnActionExpandListener(menu.findItem(R.id.search), expandListener);
    }

    private void initSearchViewTextListener(SearchView view){
        Action1<CharSequence> onNextAction = new Action1<CharSequence>() {
            @Override
            public void call(CharSequence sequence) {
                mPresenter.filterRecipes(sequence);
            }
        };
        RxSearchView.queryTextChanges(view)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNextAction);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.loadRecipes){
            if (ConnectionMonitor.checkInternetConnection(coordinatorLayout)) {
                mPresenter.loadRecipesFromNet();
            }
            return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();
    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onRecipeEvent(RecipeEvent event){
        switch(event.type){
            case RecipeEvent.TYPE_REFRESH:{
                mPresenter.setDataChanged();
                mPresenter.loadData();
                break;
            }
        }
    }

    @Override
    protected RecipeListPresenter getPresenter() {
        return new RecipeListPresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.recipe_list_fragment_layout;
    }

    @Override
    public void onDataLoadingStarted() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage(getString(R.string.recipe_loading_in_progress));
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                }
            }
        });
    }

    @Override
    public void onRecipesFiltered(ArrayList<RecipeItem> items) {
        if (items.isEmpty()){
            mPresenter.stateMachine().previousState();
            DialogManager.infoDialog(null, getString(R.string.in_stock_filter_empty_error), Dialog.DIALOG_MESSAGE).show(getFragmentManager(), "dialog");
        } else {
            adapter.setItems(items);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onStartCreateRecipeFragment(@NonNull Bundle args) {
        EventBus.getDefault().post(new DataManagerReloadEvent(DataManagerReloadEvent.TYPE_REFRESH));
        gotoActivity(getActivity(), CreateRecipeActivity.class, args);
    }


    @Override
    public void onItemsLoaded(final ArrayList<RecipeItem> items) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null){
                    progressDialog.dismiss();
                    progressDialog = null;
                }
                adapter.setItems(items);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void initFilterBtnStates(){
        if (mPresenter.stateMachine() == null) {
            mPresenter.initStateMachine();
            View.OnClickListener stateBtnClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPresenter.stateMachine().nextState();
                    mPresenter.loadData();
                }
            };
            InStockButtonFilterState allRecipesState = new InStockButtonFilterState(InStockButtonFilterState.STATE_ALL_RECIPES,
                    R.string.what_juice_can_i_make, stateBtnClickListener);
            mPresenter.stateMachine().addState(allRecipesState);

            InStockButtonFilterState inStokeState = new InStockButtonFilterState(InStockButtonFilterState.STATE_IN_STOCK_FILTER,
                    R.string.in_stock_filter_mode_cancel, stateBtnClickListener);
            mPresenter.stateMachine().addState(inStokeState);

            if (mPresenter.currentState() == null) {
                mPresenter.stateMachine().saveCurrentState(allRecipesState);
            }
        }
    }

    private void parseArguments(){
        Bundle args = getArguments();
        if (args != null){
            mPresenter.setRecipeCategory(args.getLong(KEY_RECIPE_CATEGORY, 0l));
        } else {
            mPresenter.setRecipeCategory(0l);
        }
    }
}
