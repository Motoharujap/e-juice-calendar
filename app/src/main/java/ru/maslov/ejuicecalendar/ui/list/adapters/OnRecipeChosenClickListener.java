package ru.maslov.ejuicecalendar.ui.list.adapters;

/**
 * Created by Администратор on 18.08.2016.
 */
public interface OnRecipeChosenClickListener {
    void onRecipeChosen(int itemPosition);
}
