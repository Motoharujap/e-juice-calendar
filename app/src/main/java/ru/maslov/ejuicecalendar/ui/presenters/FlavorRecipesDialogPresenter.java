package ru.maslov.ejuicecalendar.ui.presenters;

import android.os.AsyncTask;

import com.raizlabs.android.dbflow.sql.language.Condition;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.data.Query;
import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.database.model.Flavor_Table;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors;
import ru.maslov.ejuicecalendar.database.model.Recipe_Table;
import ru.maslov.ejuicecalendar.ui.dataManagers.FlavorRecipeListDataManager;
import ru.maslov.ejuicecalendar.ui.dataManagers.RecipeListDataManager;
import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;
import ru.maslov.ejuicecalendar.ui.presenters.loaders.IItemsLoadedListener;
import ru.maslov.ejuicecalendar.ui.presenters.loaders.RecipeLoaderTask;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IFlavorRecipesDialogView;

/**
 * Created by Администратор on 07.09.2016.
 */
public class FlavorRecipesDialogPresenter extends BaseRecipePresenter<IFlavorRecipesDialogView> {
    private RecipeLoaderTask task;

    @Override
    protected Class getDataManagerClass() {
        return FlavorRecipeListDataManager.class;
    }

    public void loadData(long flavorId){
        if (task == null || task != null && task.getStatus() != AsyncTask.Status.RUNNING) {
            task = new RecipeLoaderTask(new IItemsLoadedListener<RecipeItem>() {
                @Override
                public void onItemsLoaded(ArrayList<RecipeItem> items) {
                    view().onItemsLoaded(items);
                }
            }, dataManager());
            ArrayList<RecipeFlavors> recipeFlavors = JuiceDataProvider.getFlavorsCountInRecipes(flavorId);
            ArrayList<Long> recipeIds = new ArrayList<>();
            for (RecipeFlavors flavors : recipeFlavors){
                recipeIds.add(flavors.getRecipeId());
            }
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Condition.column(Recipe_Table.id.getNameAlias()).in(recipeIds));
        }
    }

    public Flavor getFlavor(long flavorID){
        return JuiceDataProvider.getFlavor(new Query(Condition.column(Flavor_Table.id.getNameAlias()).is(flavorID)));
    }
    @Override
    protected RecipeListDataManager dataManager(){
        return (RecipeListDataManager) mDataManager;
    }

    @Override
    protected IFlavorRecipesDialogView view(){
        if (mView.get() == null){
            return new IFlavorRecipesDialogView() {
                @Override
                public void onItemsLoaded(ArrayList<RecipeItem> items) {

                }

                @Override
                public String getString(int i) {
                    return "";
                }
            };
        }
        return mView.get();
    }

    @Override
    protected Condition.In params() {
        return null;
    }
}
