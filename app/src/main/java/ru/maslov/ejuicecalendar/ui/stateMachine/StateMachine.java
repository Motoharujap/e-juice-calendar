package ru.maslov.ejuicecalendar.ui.stateMachine;

import java.util.LinkedList;

/**
 * Created by Администратор on 11.09.2016.
 */
public class StateMachine<T> {
    private static final String TAG = StateMachine.class.getSimpleName();

    private static final int PREVIOUS_STATE = 0;
    private static final int NEXT_STATE = 1;

    private LinkedList<T> states = new LinkedList<>();
    private T currentState;
    private OnStateChangedListener<T> stateChangedListener;

    public void addState(T newState){
        if(!states.contains(newState)){
            states.addLast(newState);
        }
    }

    public void setStateChangedListener(OnStateChangedListener<T> stateChangedListener) {
        this.stateChangedListener = stateChangedListener;
    }

    public void nextState(){
        if (canChangeState(NEXT_STATE)){
            currentState = states.get(currentStateIndex() + 1);
        } else {
            currentState = states.getFirst();
        }
        notifyListeners(currentState);
    }

    public void previousState(){
       if (canChangeState(PREVIOUS_STATE)) {
           currentState = states.get(currentStateIndex() - 1);
       } else {
           currentState = states.getFirst();
       }
       notifyListeners(currentState);
    }

    public T currentState(){
        return currentState;
    }

    public void saveCurrentState(T state){
        currentState = state;
        notifyListeners(currentState);
    }

    private int currentStateIndex(){
        return states.indexOf(currentState);
    }

    private boolean canChangeState(int direction){
        switch (direction){
            case PREVIOUS_STATE:{
                return currentStateIndex() - 1 >= 0;
            }
            case NEXT_STATE:{
                return states.size() > currentStateIndex() + 1;
            }
            default:{
                return false;
            }
        }
    }

    private void notifyListeners(T newState){
        if (stateChangedListener != null){
            stateChangedListener.onStateChanged(newState);
        }
    }
}
