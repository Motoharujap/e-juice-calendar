package ru.maslov.ejuicecalendar.ui.presenters;

import android.os.AsyncTask;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.RecipeCategoryInfo;
import ru.maslov.ejuicecalendar.ui.dataManagers.RecipeCategoryDataManager;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IRecipeCategoryView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 21.09.2016.
 */
public class RecipeCategoryPresenter extends BasePresenter<IRecipeCategoryView> {
    private CategoryLoader loader = new CategoryLoader();
    @Override
    protected Class getDataManagerClass() {
        return RecipeCategoryDataManager.class;
    }

    public void loadData(){
        if (loader == null || loader.getStatus() != AsyncTask.Status.RUNNING) {
            loader = new CategoryLoader();
            loader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private RecipeCategoryDataManager dataManager(){
        return (RecipeCategoryDataManager) mDataManager;
    }

    protected IRecipeCategoryView view(){
        if (mView.get() == null){
            return new IRecipeCategoryView() {
                @Override
                public void onItemsLoaded(ArrayList<RecipeCategoryInfo> items) {

                }

                @Override
                public String getString(int i) {
                    return null;
                }
            };
        }
        return mView.get();
    }

    class CategoryLoader extends AsyncTask<Void, Void, ArrayList<RecipeCategoryInfo>>{

        @Override
        protected ArrayList<RecipeCategoryInfo> doInBackground(Void... params) {
            return dataManager().getObjects(null);
        }

        @Override
        protected void onPostExecute(ArrayList<RecipeCategoryInfo> recipeCategoryInfos) {
            view().onItemsLoaded(recipeCategoryInfos);
        }
    }

}
