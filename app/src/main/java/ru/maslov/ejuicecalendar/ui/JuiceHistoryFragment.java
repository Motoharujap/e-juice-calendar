package ru.maslov.ejuicecalendar.ui;

import android.widget.BaseAdapter;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.ui.dialogs.DialogManager;
import ru.maslov.ejuicecalendar.ui.list.adapters.HistoryAdapter;
import ru.maslov.ejuicecalendar.ui.presenters.EJuiceHistoryPresenter;
import ru.maslov.ejuicecalendar.ui.presenters.EjuiceSchedulePresenter;

/**
 * Created by Администратор on 01.09.2016.
 */
public class JuiceHistoryFragment extends EJuiceScheduleFragment{

    public static JuiceHistoryFragment getInstance(){
        return new JuiceHistoryFragment();
    }

    @Override
    protected BaseAdapter getGridAdapter() {
        return new HistoryAdapter(getActivity());
    }

    @Override
    protected EjuiceSchedulePresenter getPresenter() {
        return new EJuiceHistoryPresenter();
    }

    @Override
    protected boolean isFABVisible() {
        return false;
    }

    @Override
    protected String getEmptyListMessage() {
        return getString(R.string.history_fragment_empty_message);
    }

    @Override
    protected void showJuiceInfoDialog(long juiceId) {
        DialogManager.juiceHistoryInfoDialog(juiceId).show(getFragmentManager(), "dialog");
    }
}
