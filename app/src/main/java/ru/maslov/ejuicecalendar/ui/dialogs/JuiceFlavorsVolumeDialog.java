package ru.maslov.ejuicecalendar.ui.dialogs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.logic.JuiceParams;
import ru.maslov.ejuicecalendar.ui.presenters.JuiceFlavorVolumeDialogPresenter;
import ru.maslov.sandbox.dialogs.BaseDialogFragment;

/**
 * Created by Администратор on 20.08.2016.
 */
public class JuiceFlavorsVolumeDialog extends BaseDialogFragment<JuiceFlavorVolumeDialogPresenter> {
    @Bind(R.id.recipeInfoContainer) protected LinearLayout recipeComponentsInfoContainer;
    @Bind(R.id.caption) protected TextView dialogCaption;
    @Bind(R.id.okBtn)protected Button closeDialogButton;

    private JuiceParams params;

    public static final String KEY_JUICE_PARAMS = "juice_params";
    public static final String KEY_CAPTION = "caption";

    public static JuiceFlavorsVolumeDialog getInstance(@NonNull Bundle args){
        JuiceFlavorsVolumeDialog dialog = new JuiceFlavorsVolumeDialog();
        dialog.setArguments(args);
        return dialog;
    }
    @Override
    protected JuiceFlavorVolumeDialogPresenter getPresenter() {
        return new JuiceFlavorVolumeDialogPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(getLayoutResId(), container, false);
        ButterKnife.bind(this, mainView);
        parseArguments();
        fillFlavorsInfoLayout();
        closeDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JuiceFlavorsVolumeDialog.this.dismissAllowingStateLoss();
            }
        });
        return mainView;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.juice_flavor_volume_dialog_layout;
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    private void fillFlavorsInfoLayout(){
        recipeComponentsInfoContainer.removeAllViews();
        ArrayList<View> childViews = JuiceDataProvider.getFlavorDataViews(getActivity(), mPresenter, params);
        for (View view : childViews){
            recipeComponentsInfoContainer.addView(view);
        }
    }

    private void parseArguments(){
        Bundle args = getArguments();
        String caption = args.getString(KEY_CAPTION, "");
        dialogCaption.setText(caption);
        params = (JuiceParams) args.getSerializable(KEY_JUICE_PARAMS);
        mPresenter.initJuice(params.recipeId);
    }
}
