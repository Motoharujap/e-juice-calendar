package ru.maslov.ejuicecalendar.ui.dataManagers;

import com.raizlabs.android.dbflow.sql.language.Condition;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.FlavorInfo;
import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.event_bus.FlavorEvent;

/**
 * Created by Администратор on 04.09.2016.
 */
public class FlavorListDataManager extends BaseListDataManager<FlavorInfo> {

    @Override
    protected ArrayList<FlavorInfo> loadData(Condition.In condition) {
        ArrayList<FlavorInfo> flavorInfos = new ArrayList<>();
        ArrayList<Flavor> flavors = JuiceDataProvider.getFlavors();
        for (Flavor flavor : flavors){
            flavorInfos.add(new FlavorInfo(flavor));
        }
        return flavorInfos;
    }

    @Subscribe
    public void onFlavorEvent(FlavorEvent event){
        switch (event.type){
            case FlavorEvent.TYPE_REFRESH:{
                setDataChanged();
            }
        }
    }
}
