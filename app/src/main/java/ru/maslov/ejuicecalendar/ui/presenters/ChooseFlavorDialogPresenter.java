package ru.maslov.ejuicecalendar.ui.presenters;

import android.os.AsyncTask;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IChooseFlavorDialogView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 30.09.2016.
 */

public class ChooseFlavorDialogPresenter extends BasePresenter<IChooseFlavorDialogView> {
    private FlavorsLoader loader;
    @Override
    protected Class getDataManagerClass() {
        return null;
    }

    public void loadData(){
        if (loader == null || loader.getStatus() != AsyncTask.Status.RUNNING){
            loader = new FlavorsLoader();
            loader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    class FlavorsLoader extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<Flavor> flavors = JuiceDataProvider.getFlavors();
            view().onItemsLoaded(flavors);
            return null;
        }
    }

    protected IChooseFlavorDialogView view(){
        if (mView.get() == null){
            return new IChooseFlavorDialogView() {
                @Override
                public void onItemsLoaded(ArrayList<Flavor> items) {

                }

                @Override
                public String getString(int i) {
                    return null;
                }
            };
        }
        return mView.get();
    }
}
