package ru.maslov.ejuicecalendar.ui.dialogs;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.ui.list.adapters.ChooseFlavorDialogAdapter;
import ru.maslov.ejuicecalendar.ui.presenters.ChooseFlavorDialogPresenter;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IChooseFlavorDialogView;
import ru.maslov.sandbox.dialogs.BaseDialogFragment;

/**
 * Created by Администратор on 30.09.2016.
 */

public class ChooseFlavorDialogFragment extends BaseDialogFragment<ChooseFlavorDialogPresenter> implements IChooseFlavorDialogView {
    private static final String TAG = ChooseFlavorDialogFragment.class.getSimpleName();
    @Bind(R.id.list)protected ListView list;
    public static final int REQUEST_CHOOSE_FLAVOR = 100;
    public static final String KEY_CHOSEN_FLAVOR_ID = "flavor_id";
    private ChooseFlavorDialogAdapter adapter;
    @Override
    protected ChooseFlavorDialogPresenter getPresenter() {
        return new ChooseFlavorDialogPresenter();
    }

    public static ChooseFlavorDialogFragment getInstance(){
        return new ChooseFlavorDialogFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.choose_flavor_dialog_layout;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View main = inflater.inflate(getLayoutResId(), container, false);
        ButterKnife.bind(this, main);
        adapter = new ChooseFlavorDialogAdapter(getActivity());
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment targetFragment = getTargetFragment();
                if (targetFragment != null){
                    Intent data = new Intent();
                    data.putExtra(KEY_CHOSEN_FLAVOR_ID, id);
                    targetFragment.onActivityResult(REQUEST_CHOOSE_FLAVOR, Activity.RESULT_OK, data);
                    dismiss();
                } else {
                    Log.e(TAG, "CRITICAL ERROR: target fragment is null! Have you forgotten to set target fragment to this dialog?");
                }
            }
        });
        return main;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();
    }

    @Override
    public void onItemsLoaded(final ArrayList<Flavor> items) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.setItems(items);
            }
        });
    }
}
