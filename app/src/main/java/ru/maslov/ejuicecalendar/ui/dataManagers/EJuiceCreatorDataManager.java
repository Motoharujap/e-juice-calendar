package ru.maslov.ejuicecalendar.ui.dataManagers;

import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.Date;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.database.JuiceDatabase;
import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.database.model.Recipe_Table;
import ru.maslov.sandbox.dataLayer.IDataManager;

/**
 * Created by Администратор on 24.07.2016.
 */
public class EJuiceCreatorDataManager implements IDataManager {
    private Juice mJuice;

    public EJuiceCreatorDataManager() {
        createJuice(-1);
    }

    public Juice createJuice(long juiceId){
        if (juiceId == -1){
            mJuice = new Juice();
        } else {
            mJuice = JuiceDataProvider.getJuiceById(juiceId);
        }
        return mJuice;
    }



    public Juice getJuice(){
        return mJuice;
    }

    public void saveJuice(){
        if (mJuice.getCreationDate() == null) {
            Date creationDate = new Date(System.currentTimeMillis());
            mJuice.setCreationDate(creationDate);
        }
        //it's a new juice, save it
        if (mJuice.getId() == 0){
            mJuice.save();
        //juice already exists, we're just editing it so call update()
        } else {
            mJuice.update();
        }
    }

    public Recipe getRecipeById(long recipeId){
        return JuiceDataProvider.getRecipeById(recipeId);
    }

    @Override
    public void onDestroy() {
    }
}
