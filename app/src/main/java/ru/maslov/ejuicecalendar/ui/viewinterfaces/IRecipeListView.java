package ru.maslov.ejuicecalendar.ui.viewinterfaces;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;
import ru.maslov.sandbox.IView;

/**
 * Created by Администратор on 07.09.2016.
 */
public interface IRecipeListView extends IView{
    void onItemsLoaded(ArrayList<RecipeItem> items);
}
