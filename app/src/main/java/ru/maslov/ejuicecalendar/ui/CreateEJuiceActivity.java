package ru.maslov.ejuicecalendar.ui;

import android.content.Intent;
import android.os.Bundle;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.core.CoreActivity;

/**
 * Created by Администратор on 23.07.2016.
 */
public class CreateEJuiceActivity extends CoreActivity {
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        intent = getIntent();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_create_ejuice;
    }

    @Override
    protected void showMainFragment() {
        if (intent != null && intent.getLongExtra(EJuiceCreatorFragment.KEY_RECIPE_ID, 0) != 0){
            Bundle args = new Bundle();
            args.putLong(EJuiceCreatorFragment.KEY_RECIPE_ID, intent.getLongExtra(EJuiceCreatorFragment.KEY_RECIPE_ID, 0));
            startFragmentTransaction(EJuiceCreatorFragment.getInstance(args), false, false, getMainFragmentResId());
        } else {
            startFragmentTransaction(EJuiceCreatorFragment.getInstance(), false, false, getMainFragmentResId());
        }
    }

    @Override
    protected int getMainFragmentResId() {
        return R.id.fragmentContainer;
    }

    @Override
    public void onBackPressed() {
        gotoActivity(CreateEJuiceActivity.this, StartActivity.class);
        super.onBackPressed();
    }
}
