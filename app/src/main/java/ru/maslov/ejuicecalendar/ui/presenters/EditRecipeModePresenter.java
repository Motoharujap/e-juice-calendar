package ru.maslov.ejuicecalendar.ui.presenters;

import ru.maslov.ejuicecalendar.ui.viewinterfaces.ICreateRecipeFragmentView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 29.09.2016.
 */

public class EditRecipeModePresenter extends BasePresenter<ICreateRecipeFragmentView> {
    @Override
    protected Class getDataManagerClass() {
        return null;
    }
}
