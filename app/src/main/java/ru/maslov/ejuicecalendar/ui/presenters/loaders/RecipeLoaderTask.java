package ru.maslov.ejuicecalendar.ui.presenters.loaders;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.raizlabs.android.dbflow.sql.language.Condition;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.ui.dataManagers.RecipeListDataManager;
import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;

/**
 * Created by Администратор on 07.09.2016.
 */
public class RecipeLoaderTask extends AsyncTask<Condition.In, Void, ArrayList<RecipeItem>> {
    private IItemsLoadedListener<RecipeItem> itemsLoadedListener;
    private RecipeListDataManager dataManager;

    public RecipeLoaderTask(@Nullable IItemsLoadedListener<RecipeItem> itemsLoadedListener, @NonNull RecipeListDataManager dataManager) {
        this.itemsLoadedListener = itemsLoadedListener;
        this.dataManager = dataManager;
    }

    @Override
    protected ArrayList<RecipeItem> doInBackground(Condition.In... params) {
        ArrayList<RecipeItem> recipeItems = dataManager.getRecipeItems(params.length > 0 ? params[0] : null);
        return recipeItems;
    }

    @Override
    protected void onPostExecute(ArrayList<RecipeItem> recipeItems) {
        if (itemsLoadedListener != null) {
            itemsLoadedListener.onItemsLoaded(recipeItems);
        }
        dataManager = null;
    }
}
