package ru.maslov.ejuicecalendar.ui;

import android.content.Intent;
import android.os.Bundle;

import org.greenrobot.eventbus.EventBus;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.core.CoreActivity;
import ru.maslov.ejuicecalendar.ui.dataManagers.CreateRecipeDataManager;
import ru.maslov.ejuicecalendar.ui.presenters.CreateRecipeFragmentPresenter;
import ru.maslov.sandbox.eventBus.LeaveStateEvent;

/**
 * Created by Администратор on 19.08.2016.
 */
public class CreateRecipeActivity extends CoreActivity {
    private Intent intent;
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_create_recipe;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        intent = getIntent();
        super.onCreate(savedInstanceState);
    }
    @Override
    protected void showMainFragment() {
        Bundle args;
        if (intent != null && intent.getExtras() != null){
            args = intent.getExtras();
        } else {
            args = new Bundle();
            args.putInt(CreateRecipeFragment.KEY_EDITING_MODE, CreateRecipeFragmentPresenter.EditMode.MODE_CREATE_RECIPE_ALL_CATEGORIES);
        }
        startFragmentTransaction(CreateRecipeFragment.getInstance(args), false, false, R.id.fragmentContainer);
    }

    @Override
    protected int getMainFragmentResId() {
        return R.id.fragmentContainer;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        EventBus.getDefault().post(new LeaveStateEvent(CreateRecipeFragmentPresenter.class.getSimpleName()));
    }
}
