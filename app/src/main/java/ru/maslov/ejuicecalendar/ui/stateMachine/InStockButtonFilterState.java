package ru.maslov.ejuicecalendar.ui.stateMachine;

import android.view.View;

/**
 * Created by Администратор on 11.09.2016.
 */
public class InStockButtonFilterState {
    public static final int STATE_ALL_RECIPES = 0;
    public static final int STATE_IN_STOCK_FILTER = 1;

    private int btnTextResId;
    private View.OnClickListener clickListener;
    private int id;

    public InStockButtonFilterState(int id, int btnTextResId, View.OnClickListener clickListener){
        this.id = id;
        this.btnTextResId = btnTextResId;
        this.clickListener = clickListener;
    }

    public int getBtnTextResId() {
        return btnTextResId;
    }

    public View.OnClickListener getClickListener() {
        return clickListener;
    }

    public int getId() {
        return id;
    }
}
