package ru.maslov.ejuicecalendar.ui.presenters;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.data.RecipeInfo;
import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.logic.FlavorViewData;
import ru.maslov.ejuicecalendar.logic.JuiceParams;
import ru.maslov.ejuicecalendar.ui.IFlavorViewDataProvider;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IJuiceInfoDialog;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 28.07.2016.
 */
public class JuiceInfoDialogPresenter extends BasePresenter<IJuiceInfoDialog> implements IFlavorViewDataProvider{
    public Juice juice;
    private RecipeInfo recipeInfo;
    public JuiceParams params;

    public void initData(long juiceId){
        juice = getJuiceById(juiceId);
        Recipe juiceRecipe = getJuiceRecipe();
        recipeInfo = new RecipeInfo(juiceRecipe);
        params = new JuiceParams(juice.getRecipeId(), juice.getContainerVolume(), juice.getPgVolume(), juice.getVgVolume());
    }

    public String getJuiceRecipeName() {
        Recipe recipe = getJuiceRecipe();
        if (recipe != null) {
            return recipe.getRecipeName();
        }
        return "";
    }

    private Recipe getJuiceRecipe(){
        return JuiceDataProvider.getRecipeById(juice.getRecipeId());
    }

    private Juice getJuiceById(long id){
        if (juice == null) {
            juice = JuiceDataProvider.getJuiceById(id);
        }
        return juice;
    }

    public void saveRating(float rating){
        Recipe recipe = getJuiceRecipe();
        if (recipe != null){
            recipe.setRating(rating);
            recipe.update();
        }
    }

    public float getRecipeRating(){
        Recipe recipe = getJuiceRecipe();
        if (recipe != null){
            return recipe.getRating();
        }
        return 0f;
    }


    @Override
    protected Class getDataManagerClass() {
        return null;
    }

    @Override
    public ArrayList<FlavorViewData> getFlavorViewData(JuiceParams params) {
        ArrayList<FlavorViewData> data = JuiceDataProvider.getFlavorViewData(params);
        return data;
    }
}
