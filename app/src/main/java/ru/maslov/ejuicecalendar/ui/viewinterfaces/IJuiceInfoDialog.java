package ru.maslov.ejuicecalendar.ui.viewinterfaces;

import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.sandbox.IView;

/**
 * Created by Администратор on 28.07.2016.
 */
public interface IJuiceInfoDialog extends IView{
    void onJuiceInfoReady(Juice juice);
}
