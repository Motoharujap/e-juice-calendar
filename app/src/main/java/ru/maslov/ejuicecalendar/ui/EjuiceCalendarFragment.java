package ru.maslov.ejuicecalendar.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.imanoweb.calendarview.CalendarListener;
import com.imanoweb.calendarview.CustomCalendarView;
import com.imanoweb.calendarview.DayDecorator;
import com.imanoweb.calendarview.DayView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.event_bus.CalendarEvent;
import ru.maslov.ejuicecalendar.ui.dialogs.DialogManager;
import ru.maslov.ejuicecalendar.ui.dialogs.JuiceInfoDialog;
import ru.maslov.ejuicecalendar.ui.presenters.EjuiceCalendarPresenter;
import ru.maslov.sandbox.BaseFragment;
import ru.maslov.sandbox.dialogs.Dialog;

/**
 * Created by Администратор on 23.07.2016.
 */
public class EjuiceCalendarFragment extends BaseFragment<EjuiceCalendarPresenter> {
    @Bind(R.id.calendar_view)protected CustomCalendarView mCalendarView;

    private Calendar mCalendar;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = super.onCreateView(inflater, container, savedInstanceState);
        mPresenter.loadData();
        DayDecorator dayDecorator = new DayDecorator() {
            @Override
            public void decorate(DayView dayView) {
                if (!mPresenter.getJuicesAtDate(dayView.getDate()).isEmpty()){
                    dayView.setBackgroundColor(getResources().getColor(R.color.material_red_400));
                }
            }
        };

        final Calendar calendar = mCalendarView.getCurrentCalendar();
        ArrayList<DayDecorator> decorators = new ArrayList<>();
        decorators.add(dayDecorator);
        mCalendarView.setDecorators(decorators);
        mCalendarView.refreshCalendar(calendar);
        mCalendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                final ArrayList<Juice> juicesAtDate = mPresenter.getJuicesAtDate(date);
                if (!juicesAtDate.isEmpty()) {
                    CharSequence[] names = mPresenter.getJuicesNames(juicesAtDate);
                    DialogManager.listDialog(null, Dialog.DIALOG_SINGLECHOICE, names, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Bundle args = new Bundle();
                            args.putLong(JuiceInfoDialog.KEY_JUICE_ID, juicesAtDate.get(which).getId());
                            JuiceInfoDialog infoDialog = JuiceInfoDialog.getInstance(args);
                            infoDialog.show(getFragmentManager(), "dialog");
                        }
                    }).show(getFragmentManager(), "dialog");
                }
            }

            @Override
            public void onMonthChanged(Date date) {

            }
        });
        mCalendar = mCalendarView.getCurrentCalendar();
        return mainView;
    }

    @Subscribe
    public void onCalendarEvent(CalendarEvent event){
        switch(event.type){
            case CalendarEvent.TYPE_REFRESH:{
                mCalendarView.refreshCalendar(mCalendarView.getCurrentCalendar());
            }
        }
    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public static EjuiceCalendarFragment getInstance(){
        return new EjuiceCalendarFragment();
    }
    @Override
    protected EjuiceCalendarPresenter getPresenter() {
        return new EjuiceCalendarPresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.ejuice_calendar_fragment;
    }
}
