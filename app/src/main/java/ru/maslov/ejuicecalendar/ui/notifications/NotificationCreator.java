package ru.maslov.ejuicecalendar.ui.notifications;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.support.annotation.Nullable;

import ru.maslov.ejuicecalendar.R;

/**
 * Created by Администратор on 31.08.2016.
 */
public class NotificationCreator {

    public static Notification simpleActionNotification(Context context, @Nullable String message, @Nullable  String title, PendingIntent action){
        Notification.Builder builder = new Notification.Builder(context);
        if (message != null){
            builder.setContentText(message);
        }
        if (title != null){
            builder.setContentTitle(title);
        }
        builder.setContentIntent(action);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setAutoCancel(true);
        return builder.build();
    }
}
