package ru.maslov.ejuicecalendar.ui;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.logic.FlavorViewData;
import ru.maslov.ejuicecalendar.logic.JuiceParams;

/**
 * Created by Администратор on 29.08.2016.
 */
public interface IFlavorViewDataProvider {
    ArrayList<FlavorViewData> getFlavorViewData(JuiceParams params);
}
