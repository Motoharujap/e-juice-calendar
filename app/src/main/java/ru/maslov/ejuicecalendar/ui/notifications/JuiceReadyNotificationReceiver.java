package ru.maslov.ejuicecalendar.ui.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.greenrobot.eventbus.EventBus;

import ru.maslov.ejuicecalendar.core.EJuiceApplication;
import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.event_bus.JuiceEvent;
import ru.maslov.ejuicecalendar.event_bus.RefreshEvent;
import ru.maslov.ejuicecalendar.tools.RemoteLogger;

/**
 * Created by Администратор on 31.08.2016.
 */
public class JuiceReadyNotificationReceiver extends BroadcastReceiver {
    private static final String TAG = JuiceReadyNotificationReceiver.class.getSimpleName();

    public static final String NOTIFICATION = "notification";
    public static final String JUICE_ID = "notification_id";
    @Override
    public void onReceive(Context context, Intent intent) {
        RemoteLogger.appendLog(EJuiceApplication.getInstance(), "Juice notification received");
        NotificationManager manager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        long notificationId = intent.getLongExtra(JUICE_ID, -1);
        if (notificationId != -1) {
            Juice juice = JuiceDataProvider.getJuiceById(notificationId);
            Recipe recipe = JuiceDataProvider.getRecipeById(juice.getRecipeId());
            if (!juice.equals(Juice.NULL) && !recipe.equals(Recipe.NULL)){
                updateJuiceStatus(juice);
                notifyJuiceDataChanged();
                manager.notify((int) notificationId, notification);
            } else {
                RemoteLogger.appendLog(EJuiceApplication.getInstance(), "Juice is null!");
            }
        }
    }

    private void updateJuiceStatus(Juice juice){
        juice.setIsFinished(1);
        juice.update();
    }

    private void notifyJuiceDataChanged(){
        EventBus.getDefault().post(new JuiceEvent(RefreshEvent.TYPE_REFRESH, null));
    }
}
