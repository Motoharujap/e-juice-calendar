package ru.maslov.ejuicecalendar.ui.viewinterfaces;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.ui.list.listItems.FlavorListItem;
import ru.maslov.sandbox.IView;

/**
 * Created by Администратор on 03.09.2016.
 */
public interface IFlavorListView extends IView {

    void onItemsLoaded(ArrayList<FlavorListItem> items);
}
