package ru.maslov.ejuicecalendar.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.Bind;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.data.RecipeCategoryInfo;
import ru.maslov.ejuicecalendar.event_bus.RecipeEvent;
import ru.maslov.ejuicecalendar.event_bus.RefreshEvent;
import ru.maslov.ejuicecalendar.ui.list.adapters.RecipeCategoriesGridAdapter;
import ru.maslov.ejuicecalendar.ui.list.adapters.RecipeListAdapter;
import ru.maslov.ejuicecalendar.ui.presenters.RecipeCategoryPresenter;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IRecipeCategoryView;
import ru.maslov.sandbox.BaseFragment;

/**
 * Created by Администратор on 21.09.2016.
 */
public class RecipeCategoryFragment extends BaseFragment<RecipeCategoryPresenter> implements IRecipeCategoryView{
    @Bind(R.id.categoryGridView)protected GridView gridView;
    @Bind(R.id.fab)protected FloatingActionButton fab;

    private RecipeCategoriesGridAdapter adapter;

    public static RecipeCategoryFragment getInstance(){
        return new RecipeCategoryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView =  super.onCreateView(inflater, container, savedInstanceState);
        adapter = new RecipeCategoriesGridAdapter(getActivity());
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle args = new Bundle();
                args.putLong(RecipeListFragment.KEY_RECIPE_CATEGORY, id);
                RecipeListFragment recipeListFragment = RecipeListFragment.getInstance(args);
                startFragmentTransaction(recipeListFragment, true, true, R.id.fragmentContainer);
                EventBus.getDefault().post(new RecipeEvent(RecipeEvent.TYPE_REFRESH));
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoActivity(getActivity(), CreateRecipeActivity.class);
            }
        });
        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData();
    }

    @Override
    protected RecipeCategoryPresenter getPresenter() {
        return new RecipeCategoryPresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.recipe_category_fragment;
    }


    @Override
    public void onItemsLoaded(ArrayList<RecipeCategoryInfo> items) {
        adapter.setItems(items);
    }
}
