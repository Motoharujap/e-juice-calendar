package ru.maslov.ejuicecalendar.ui.list.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.data.RecipeCategoryInfo;

/**
 * Created by Администратор on 21.09.2016.
 */
public class RecipeCategoriesGridAdapter extends BaseAdapter {
    private ArrayList<RecipeCategoryInfo> list = new ArrayList<>();
    private Context context;

    public RecipeCategoriesGridAdapter(Context context){
        this.context = context;
    }

    public void setItems(ArrayList<RecipeCategoryInfo> list){
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((RecipeCategoryInfo)getItem(position)).category().getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.recipe_category_list_item, null);
            holder.categoryName = (TextView) convertView.findViewById(R.id.recipeCategoryName);
            holder.categoryImage = (ImageView) convertView.findViewById(R.id.categoryImage);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        RecipeCategoryInfo item = (RecipeCategoryInfo) getItem(position);
        holder.categoryName.setText(item.category().localizedName());
        holder.categoryImage.setImageDrawable(context.getDrawable(item.category().getPictureResId()));
        return convertView;
    }

    class ViewHolder {
        TextView categoryName;
        ImageView categoryImage;
    }
}
