package ru.maslov.ejuicecalendar.ui.viewinterfaces;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.ui.list.listItems.ScheduleItem;
import ru.maslov.sandbox.IView;

/**
 * Created by Администратор on 23.07.2016.
 */
public interface IEjuiceScheduleView extends IView {
    void onItemsLoaded(ArrayList<ScheduleItem> items);
}
