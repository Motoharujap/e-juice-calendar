package ru.maslov.ejuicecalendar.ui.list.adapters;

import android.content.Context;

import ru.maslov.ejuicecalendar.R;

/**
 * Created by Администратор on 02.09.2016.
 */
public class HistoryAdapter extends ScheduleAdapter {

    public HistoryAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    protected int getItemBackgroundColor() {
        return mContext.getResources().getColor(R.color.material_light_green_600);
    }
}
