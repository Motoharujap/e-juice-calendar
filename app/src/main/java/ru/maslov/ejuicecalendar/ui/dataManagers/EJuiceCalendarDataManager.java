package ru.maslov.ejuicecalendar.ui.dataManagers;

import com.raizlabs.android.dbflow.sql.language.Select;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.event_bus.CalendarEvent;
import ru.maslov.ejuicecalendar.event_bus.JuiceEvent;
import ru.maslov.sandbox.dataLayer.IDataManager;

/**
 * Created by Администратор on 30.07.2016.
 */
public class EJuiceCalendarDataManager implements IDataManager {
    private ArrayList<Juice> mJuices = new ArrayList<>();
    private boolean mIsRefreshRequired = true;

    public EJuiceCalendarDataManager(){
        EventBus.getDefault().register(this);
    }
    public void loadData(){
        if (mIsRefreshRequired){
            mJuices = (ArrayList<Juice>) new Select().from(Juice.class).queryList();
            mIsRefreshRequired = false;
        }
    }
    public ArrayList<Juice> getJuices(){
        loadData();
        return mJuices;
    }

    @Subscribe
    public void onJuiceEvent(JuiceEvent event){
        switch (event.type){
            case JuiceEvent.TYPE_REFRESH:{
                setDataChanged();
                loadData();
                EventBus.getDefault().post(new CalendarEvent(CalendarEvent.TYPE_REFRESH));
            }
        }
    }

    public void setDataChanged(){
        mIsRefreshRequired = true;
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }
}
