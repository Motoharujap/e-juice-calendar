package ru.maslov.ejuicecalendar.ui.presenters.loaders;

import java.util.ArrayList;

/**
 * Created by Администратор on 07.09.2016.
 */
public interface IItemsLoadedListener<T> {
    void onItemsLoaded(ArrayList<T> items);
}
