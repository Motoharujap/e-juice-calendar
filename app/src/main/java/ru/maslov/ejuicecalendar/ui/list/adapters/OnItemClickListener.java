package ru.maslov.ejuicecalendar.ui.list.adapters;

import android.view.View;

/**
 * Created by Администратор on 29.07.2016.
 */
public interface OnItemClickListener {
    void onItemClick(View v, int position);
}
