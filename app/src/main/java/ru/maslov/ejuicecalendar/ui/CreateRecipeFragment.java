package ru.maslov.ejuicecalendar.ui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.Bind;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.logic.RecipeFlavorsViewData;
import ru.maslov.ejuicecalendar.ui.dialogs.ChooseFlavorDialogFragment;
import ru.maslov.ejuicecalendar.ui.dialogs.DialogManager;
import ru.maslov.ejuicecalendar.ui.presenters.CreateRecipeFragmentPresenter;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.ICreateRecipeFragmentView;
import ru.maslov.sandbox.BaseFragment;
import ru.maslov.sandbox.dialogs.Dialog;

/**
 * Created by Администратор on 19.08.2016.
 */
public class CreateRecipeFragment extends BaseFragment<CreateRecipeFragmentPresenter> implements ICreateRecipeFragmentView{
    private static final String TAG = CreateRecipeFragment.class.getSimpleName();

    @Bind(R.id.juiceName)protected EditText recipeName;
    @Bind(R.id.addFlavorBtn)protected Button addFlavorBtn;
    @Bind(R.id.flavorsLayout) protected LinearLayout flavorsContainer;
    @Bind(R.id.decoctionPeriod) protected EditText decoctionPeriod;
    @Bind(R.id.categorySpinner) protected Spinner categorySpinner;
    @Bind(R.id.fab)protected FloatingActionButton fab;

    public static final String KEY_RECIPE_ID = "recipe_id";
    public static final String KEY_CATEGORY_ID = "category_id";
    public static final String KEY_EDITING_MODE = "editing_mode";

    private FlavorChosenCallback flavorChosenCallback;

    public static CreateRecipeFragment getInstance(@NonNull Bundle args){
        CreateRecipeFragment fragment = new CreateRecipeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = super.onCreateView(inflater, container, savedInstanceState);
        parseArguments();
        mPresenter.initializeModes();
        mPresenter.editMode().initFlavorViewData();
        addFlavorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.addRecipeFlavorsToSave(new RecipeFlavorsViewData());
                fillFlavorContainer();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFieldsValid()){
                    showInvalidFieldsErrorDialog();
                } else {
                    saveRecipe();
                    leaveFragment();
                }
            }
        });
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, mPresenter.categoriesNames());
        categorySpinner.setAdapter(adapter);
        categorySpinner.setSelection(mPresenter.editMode().categoryListPosition());
        recipeName.setText(mPresenter.editMode().recipeName());
        decoctionPeriod.setText(String.valueOf(mPresenter.editMode().decoctionPeriodLength()));
        //TODO call this from presenter
        fillFlavorContainer();
        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.initializeModes();
    }

    private boolean isFieldsValid(){
        return !recipeName.getText().toString().trim().equals("") &&
                mPresenter.isFlavorsFilled();
    }

    private void saveRecipe(){
        String recipeNameText = recipeName.getText().toString().trim();
        String decoctionPeriodString = decoctionPeriod.getText().toString().trim();
        int decoctionPeriodInt = decoctionPeriodString.trim().equals("") ? 0 : Integer.parseInt(decoctionPeriodString.replaceAll("\\D+", ""));
        mPresenter.saveRecipe(recipeNameText, decoctionPeriodInt, categorySpinner.getSelectedItemPosition());
    }

    private void leaveFragment(){
        Toast.makeText(getActivity(), getString(R.string.recipe_saved_message), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
        mPresenter.leaveState();
    }

    private void showInvalidFieldsErrorDialog(){
        DialogManager.confirmDialog(getString(R.string.field_is_empty_caption), String.format(getString(R.string.field_is_empty_error), ""),
                Dialog.DIALOG_CONFIRM, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                },
                null).show(getFragmentManager(), "dialog");
    }

    private void fillFlavorContainer(){
        flavorsContainer.removeAllViews();
        final ArrayList<RecipeFlavorsViewData> flavorsData = mPresenter.getRecipeFlavorsExceptDelete();
        for (final RecipeFlavorsViewData data : flavorsData){
            final View flavorView = LayoutInflater.from(getActivity()).inflate(R.layout.add_flavor_layout, null);
            final EditText flavorName = (EditText) flavorView.findViewById(R.id.flavorName);
            flavorName.setText(mPresenter.getFlavorName(data));
            flavorName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    mPresenter.onFlavorNameChanged(data, s.toString());
                }
            });
            final EditText flavorVolume = (EditText) flavorView.findViewById(R.id.flavorPercentage);
            if (data.getRecipeFlavors().getVolume() == 0){
                flavorVolume.setHint(String.valueOf(data.getRecipeFlavors().getVolume()));
            } else {
                flavorVolume.setText(String.valueOf(data.getRecipeFlavors().getVolume()));
            }
            flavorVolume.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    //no parse exception here I think as long as the type of the field is numeric decimal
                    if (!s.toString().trim().equals("")) {
                        data.getRecipeFlavors().setVolume(Double.parseDouble(s.toString().replaceAll("[^\\.0123456789]", "")));
                    }
                }
            });
            ImageButton flavorsList = (ImageButton) flavorView.findViewById(R.id.flavorList);
            flavorsList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ChooseFlavorDialogFragment fragment = ChooseFlavorDialogFragment.getInstance();
                    fragment.setTargetFragment(CreateRecipeFragment.this, ChooseFlavorDialogFragment.REQUEST_CHOOSE_FLAVOR);
                    fragment.show(getFragmentManager(), "dialog");
                    flavorChosenCallback = new FlavorChosenCallback() {
                        @Override
                        public void onFlavorChosen(long flavorId) {
                            mPresenter.updateRecipeFlavor(data, flavorId);
                            flavorName.setText(mPresenter.onFlavorChosen(flavorId));
                        }
                    };
                }
            });
            ImageButton deleteAromaBtn = (ImageButton) flavorView.findViewById(R.id.deleteAromaBtn);
            deleteAromaBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flavorsContainer.removeView(flavorView);
                    flavorsContainer.invalidate();
                    mPresenter.addRecipeFlavorsToDelete(data);
                }
            });
            flavorsContainer.addView(flavorView);
            if (flavorsData.indexOf(data) == flavorsData.size() - 1){
                flavorName.requestFocus();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ChooseFlavorDialogFragment.REQUEST_CHOOSE_FLAVOR:{
                if (resultCode == Activity.RESULT_OK){
                    flavorChosenCallback.onFlavorChosen(data.getLongExtra(ChooseFlavorDialogFragment.KEY_CHOSEN_FLAVOR_ID, 0));
                } else {
                  Log.e(TAG, "onActivityResult(), something went wrong. Result code is " + String.valueOf(resultCode));
                }
            }
        }
    }

    @Override
    protected CreateRecipeFragmentPresenter getPresenter() {
        return new CreateRecipeFragmentPresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.create_recipe_fragment_layout;
    }

    private void parseArguments(){
        Bundle args = getArguments();
        if (args.containsKey(KEY_EDITING_MODE)){
            mPresenter.setEditMode(args.getInt(KEY_EDITING_MODE));
            if (args.containsKey(KEY_RECIPE_ID)){
                long recipeId = args.getLong(KEY_RECIPE_ID);
                mPresenter.initRecipe(recipeId);
            }
        }
        if (args.containsKey(KEY_CATEGORY_ID)){
            mPresenter.setCategoryId(args.getLong(KEY_CATEGORY_ID));
        }
    }

    private interface FlavorChosenCallback {
        void onFlavorChosen(long flavorId);
    }
}
