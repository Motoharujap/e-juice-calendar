package ru.maslov.ejuicecalendar.ui.stateMachine;

/**
 * Created by Администратор on 12.09.2016.
 */
public interface OnStateChangedListener<T> {
    void onStateChanged(T newState);
}
