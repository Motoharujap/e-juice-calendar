package ru.maslov.ejuicecalendar.ui.list.adapters;

/**
 * Created by Администратор on 07.09.2016.
 */
public interface OnItemChosenListener<T> {
    void onItemChosen(T item);
}
