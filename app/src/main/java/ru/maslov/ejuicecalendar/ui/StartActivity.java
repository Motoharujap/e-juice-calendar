package ru.maslov.ejuicecalendar.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.Bind;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.core.CoreActivity;
import ru.maslov.ejuicecalendar.core.EJuiceApplication;
import ru.maslov.ejuicecalendar.event_bus.DataManagerReloadEvent;
import ru.maslov.ejuicecalendar.event_bus.RefreshEvent;
import ru.maslov.ejuicecalendar.ui.dialogs.DialogManager;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IStartActivityView;
import ru.maslov.sandbox.dialogs.Dialog;


public class StartActivity extends CoreActivity implements IStartActivityView {
    public static final String KEY_JUICE_ID = "juice_id";

    private static final int POSITION_SCHEDULE = 0;
    private static final int POSITION_CALENDAR = 1;
    private static final int POSITION_HISTORY = 2;

    @Bind(R.id.tabs)protected TabLayout mTabLayout;
    @Bind(R.id.viewpager) protected ViewPager mViewPager;
    @Bind(R.id.coordinator)protected CoordinatorLayout coordinatorLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PagerAdapter pagerAdapter = new PagerAdapter(getFragmentManager());
        pagerAdapter.addFragment(EJuiceScheduleFragment.getInstance(), getString(R.string.juice_list));
        pagerAdapter.addFragment(EjuiceCalendarFragment.getInstance(), getString(R.string.calendar));
        pagerAdapter.addFragment(JuiceHistoryFragment.getInstance(), getString(R.string.juice_history_page_caption));
        mViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        Intent openJuiceInfoIntent = getIntent();
        if (openJuiceInfoIntent != null){
            long juiceId = openJuiceInfoIntent.getLongExtra(KEY_JUICE_ID, 0);
            if (juiceId != 0) {
                mViewPager.setCurrentItem(POSITION_HISTORY);
                DialogManager.juiceHistoryInfoDialog(juiceId).show(getFragmentManager(), "dialog");
            }
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_start;
    }

    @Override
    protected int getMainFragmentResId() {
        return R.id.fragmentContainer;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_db_to_sd || id == R.id.action_db_to_app) {
            if (checkPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, 0, Build.VERSION_CODES.M)){
                try {
                    EJuiceApplication.getInstance().copyDb(id);
                    onDbCopySuccess();
                } catch (IOException e) {
                    onDbCopyToSDCardFailed();
                    e.printStackTrace();
                }
            }
        }
        if (id == R.id.action_submit_bugreport){

        }
        return super.onOptionsItemSelected(item);
    }

    private void onDbCopyToSDCardFailed() {
        Snackbar.make(coordinatorLayout, getString(R.string.db_copy_error), Snackbar.LENGTH_LONG).show();
    }

    private void onDbCopySuccess() {
        Snackbar.make(coordinatorLayout, getString(R.string.db_copy_success), Snackbar.LENGTH_LONG).show();
        EventBus.getDefault().post(new DataManagerReloadEvent(RefreshEvent.TYPE_REFRESH));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                EJuiceApplication.getInstance().copyDBFromSDCard();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Snackbar.make(coordinatorLayout, getString(R.string.permission_grant_error), Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener positiveClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                System.exit(0);
            }
        };
        DialogManager.confirmDialog(getString(R.string.confirm_action), getString(R.string.exit_app_dialog_caption), Dialog.DIALOG_CONFIRM, positiveClick, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show(StartActivity.this.getFragmentManager(), DIALOG);
    }

    @Override
    protected void showMainFragment(){
        //do nothing, we have view pager here
    }

}
