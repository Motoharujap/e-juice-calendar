package ru.maslov.ejuicecalendar.ui.list.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.ui.list.listItems.FlavorListItem;

/**
 * Created by Администратор on 03.09.2016.
 */
public class FlavorListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<FlavorListItem> flavors = new ArrayList<>();
    private OnMenuItemClickListener onMenuItemClickListener;
    private OnItemClickListener onItemClickListener;

    public FlavorListAdapter(Context context){
        this.context = context;
    }

    public void setItems(ArrayList<FlavorListItem> items){
        flavors.clear();
        flavors.addAll(items);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        this.onMenuItemClickListener = onMenuItemClickListener;
    }

    @Override
    public int getCount() {
        return flavors.size();
    }

    @Override
    public Object getItem(int position) {
        return flavors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.flavor_list_item, null);
            holder = new ViewHolder();
            holder.flavorName = (TextView) convertView.findViewById(R.id.flavorName);
            holder.recipeCount = (TextView) convertView.findViewById(R.id.recipeInfo);
            holder.averageVolume = (TextView) convertView.findViewById(R.id.averageVolume);
            holder.overflowMenu = (ImageButton) convertView.findViewById(R.id.overflowMenu);
            holder.inStockCheck = (CheckBox) convertView.findViewById(R.id.inStockCheck);
            holder.containerLayout = (LinearLayout) convertView.findViewById(R.id.containerLayout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final FlavorListItem flavorItem = (FlavorListItem) getItem(position);
        holder.flavorName.setText(flavorItem.flavor.getName());
        holder.recipeCount.setText(String.valueOf(flavorItem.recipesCount));
        holder.averageVolume.setText(String.format("%.2f", flavorItem.averageVolume) + "%");
        holder.overflowMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(context, v);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        onMenuItemClickListener.onMenuItemSelected(item, position);
                        return true;
                    }
                });
                menu.inflate(R.menu.flavor_item_menu);
                menu.show();
            }
        });
        if (flavorItem.flavor.getIsInStock() == 1){
            holder.inStockCheck.setChecked(true);
        } else {
            holder.inStockCheck.setChecked(false);
        }
        holder.inStockCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    flavorItem.flavor.setIsInStock(isChecked ? 1 : 0);
                    flavorItem.flavor.silentUpdate();
                }
            }
        });
        holder.containerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(v, position);
            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView flavorName;
        TextView recipeCount;
        TextView averageVolume;
        ImageButton overflowMenu;
        CheckBox inStockCheck;
        LinearLayout containerLayout;
    }
}
