package ru.maslov.ejuicecalendar.ui.list.adapters;

import android.content.Context;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Администратор on 07.09.2016.
 */
public abstract class CoreAdapter<T> extends BaseAdapter {
    protected Context context;
    protected ArrayList<T> items = new ArrayList<>();

    public CoreAdapter(Context context){
        this.context = context;
    }

    public void setItems(ArrayList<T> items){
        this.items.clear();
        this.items.addAll(items);
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
