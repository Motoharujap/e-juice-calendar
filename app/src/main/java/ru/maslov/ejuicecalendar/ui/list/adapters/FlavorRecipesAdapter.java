package ru.maslov.ejuicecalendar.ui.list.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.ui.list.RecipeFlavorsLayoutGenerator;
import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;

/**
 * Created by Администратор on 07.09.2016.
 */
public class FlavorRecipesAdapter extends CoreAdapter<RecipeItem> {
    private OnItemChosenListener<RecipeItem> onJuiceCreateListener;


    public FlavorRecipesAdapter(Context context) {
        super(context);
    }

    public void setOnJuiceCreateListener(OnItemChosenListener onJuiceCreateListener) {
        this.onJuiceCreateListener = onJuiceCreateListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.flavor_recipe_list_item, null);
            holder.recipeName = (TextView) convertView.findViewById(R.id.recipeName);
            holder.createJuiceBtn = (Button) convertView.findViewById(R.id.createJuiceBtn);
            holder.recipeFlavorsLayout = (LinearLayout) convertView.findViewById(R.id.recipeFlavorsLayout);
            holder.mainLayout = (RelativeLayout) convertView.findViewById(R.id.recipeInfoLayout);
            holder.recipeRating = (RatingBar) convertView.findViewById(R.id.recipeRating);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final RecipeItem item = (RecipeItem) getItem(position);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.recipeFlavorsLayout.getVisibility() == View.GONE){
                    holder.recipeFlavorsLayout.setVisibility(View.VISIBLE);
                } else {
                    holder.recipeFlavorsLayout.setVisibility(View.GONE);
                }
            }
        });
        holder.createJuiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onJuiceCreateListener.onItemChosen(item);
            }
        });
        holder.recipeName.setText(item.recipe.getRecipeName());
        holder.recipeRating.setRating(item.recipe.getRating());
        RecipeFlavorsLayoutGenerator.generateFlavorsLayout(context, holder.recipeFlavorsLayout, item.flavorItems);
        return convertView;
    }

    class ViewHolder {
        TextView recipeName;
        RatingBar recipeRating;
        RelativeLayout mainLayout;
        LinearLayout recipeFlavorsLayout;
        Button createJuiceBtn;
    }
}
