package ru.maslov.ejuicecalendar.ui.presenters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.raizlabs.android.dbflow.sql.language.Condition;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.RecipeInfo;
import ru.maslov.ejuicecalendar.database.model.RecipeCategory;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors;
import ru.maslov.ejuicecalendar.database.model.Recipe_Table;
import ru.maslov.ejuicecalendar.network.rss.RecipesRetriever;
import ru.maslov.ejuicecalendar.tools.StringTools;
import ru.maslov.ejuicecalendar.ui.CreateRecipeFragment;
import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;
import ru.maslov.ejuicecalendar.ui.stateMachine.InStockButtonFilterState;
import ru.maslov.ejuicecalendar.ui.stateMachine.OnStateChangedListener;
import ru.maslov.ejuicecalendar.ui.stateMachine.StateMachine;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IRecipeListFragmentView;

/**
 * Created by Администратор on 17.08.2016.
 */
public class RecipeListPresenter extends BaseRecipePresenter<IRecipeListFragmentView> {
    private StateMachine<InStockButtonFilterState> filterButtonStateMachine;

    private long recipeCategory;

    public void setDataChanged(){
        dataManager().setDataChanged();
    }

    public void filterRecipes(CharSequence sequence) {
        if (StringTools.isEmptyString(sequence)) {
            loadData();
            return;
        }
        ArrayList<RecipeItem> recipeItems = recipeItems();
        ArrayList<RecipeItem> filteredItems = new ArrayList<>();
        for (RecipeItem item : recipeItems){
            if (item.matchesSearchString(sequence)){
                filteredItems.add(item);
            }
        }
        view().onItemsLoaded(filteredItems);
    }

    public void initStateMachine(){
        filterButtonStateMachine = new StateMachine<>();
    }

    public StateMachine<InStockButtonFilterState> stateMachine(){
        return filterButtonStateMachine;
    }

    public void setRecipeCategory(long recipeCategory){
        this.recipeCategory = recipeCategory;
    }

    public void setStateChangedListener(OnStateChangedListener<InStockButtonFilterState> stateChangedListener){
        stateMachine().setStateChangedListener(stateChangedListener);
    }

    public void saveCurrentFilterBtnState(InStockButtonFilterState state){
        filterButtonStateMachine.saveCurrentState(state);
    }

    public InStockButtonFilterState currentState(){
        return filterButtonStateMachine.currentState();
    }

    public void editRecipe(int recipePosition){
        Bundle args = new Bundle();
        RecipeInfo info = dataManager().getObjectByPosition(recipePosition);
        args.putLong(CreateRecipeFragment.KEY_RECIPE_ID, info.recipe.getId());
        args.putInt(CreateRecipeFragment.KEY_EDITING_MODE, CreateRecipeFragmentPresenter.EditMode.MODE_EDIT_RECIPE);
        view().onStartCreateRecipeFragment(args);
    }

    public void onFabClicked(){
        Bundle args = new Bundle();
        if (recipeCategory != RecipeCategory.CATEGORY_ALL_RECIPES_ID) {
            args.putLong(CreateRecipeFragment.KEY_CATEGORY_ID, recipeCategory);
            args.putInt(CreateRecipeFragment.KEY_EDITING_MODE, CreateRecipeFragmentPresenter.EditMode.MODE_CREATE_RECIPE_CHOSEN_CATEGORY);
        } else {
            args.putInt(CreateRecipeFragment.KEY_EDITING_MODE, CreateRecipeFragmentPresenter.EditMode.MODE_CREATE_RECIPE_ALL_CATEGORIES);
        }
        view().onStartCreateRecipeFragment(args);
    }

    public void filterRecipesByFlavorsInStock(){
        ArrayList<RecipeItem> recipeItems = recipeItems();
        ArrayList<RecipeItem> filteredItems = new ArrayList<>();
        for (RecipeItem item : recipeItems){
            boolean add = true;
            for (RecipeFlavors flavorItem : item.flavorItems){
                if (flavorItem.getFlavor().getIsInStock() == 0){
                    add = false;
                }
            }
            if (add){
                filteredItems.add(item);
            }
        }
        view().onRecipesFiltered(filteredItems);
    }

    public void deleteRecipe(int position){
        dataManager().deleteItem(position);
        loadData();
    }

    public void loadRecipesFromNet(){
        view().onDataLoadingStarted();
        RecipesRetriever recipesRetriever = new RecipesRetriever();
        recipesRetriever.run();
    }

    public RecipeInfo getRecipeInfoByListPosition(int listPosition){
        return dataManager().getObjectByPosition(listPosition);
    }

    @Override
    public void loadData() {
        if (filterButtonStateMachine != null && filterButtonStateMachine.currentState() != null){
            switch (filterButtonStateMachine.currentState().getId()){
                case InStockButtonFilterState.STATE_ALL_RECIPES:{
                    super.loadData();
                    break;
                }
                case InStockButtonFilterState.STATE_IN_STOCK_FILTER:{
                    filterRecipesByFlavorsInStock();
                    break;
                }
            }
        }
    }

    @Override
    protected IRecipeListFragmentView view(){
        if (mView.get() == null){
            return new IRecipeListFragmentView() {
                @Override
                public void onDataLoadingStarted() {

                }

                @Override
                public void onRecipesFiltered(ArrayList<RecipeItem> items) {

                }

                @Override
                public void onStartCreateRecipeFragment(@NonNull Bundle args) {

                }

                @Override
                public void onItemsLoaded(ArrayList<RecipeItem> items) {

                }

                @Override
                public String getString(int i) {
                    return "";
                }
            };
        }
        return mView.get();
    }

    @Override
    protected Condition.In params() {
        return condition();
    }

    private ArrayList<RecipeItem> recipeItems(){
        return dataManager().getRecipeItems(condition());
    }

    @Nullable
    private Condition.In condition(){
        return recipeCategory != 0l ? Condition.column(Recipe_Table.categoryId.getNameAlias()).in(recipeCategory) : null;
    }
}
