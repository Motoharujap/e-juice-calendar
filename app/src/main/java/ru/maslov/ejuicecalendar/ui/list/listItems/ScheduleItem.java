package ru.maslov.ejuicecalendar.ui.list.listItems;

import java.util.Date;

import ru.maslov.ejuicecalendar.miscellaneous.Strings;
import ru.maslov.ejuicecalendar.tools.DateTools;

/**
 * Created by Администратор on 23.07.2016.
 */
public class ScheduleItem {
    private String mJuiceName = Strings.UNDEFINED;
    private String mReadyAt = Strings.UNDEFINED;

    public ScheduleItem(String mJuiceName, Date mReadyAt) {
        this.mJuiceName = mJuiceName;
        this.mReadyAt = DateTools.dateToString(mReadyAt, DateTools.JUST_DATE);
    }

    public String getJuiceName() {
        return mJuiceName;
    }

    public String getReadyIn() {
        return mReadyAt;
    }
}
