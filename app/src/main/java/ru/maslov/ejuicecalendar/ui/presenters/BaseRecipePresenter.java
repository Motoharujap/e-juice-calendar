package ru.maslov.ejuicecalendar.ui.presenters;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;

import ru.maslov.ejuicecalendar.ui.dataManagers.RecipeListDataManager;
import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;
import ru.maslov.ejuicecalendar.ui.presenters.loaders.IItemsLoadedListener;
import ru.maslov.ejuicecalendar.ui.presenters.loaders.RecipeLoaderTask;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IRecipeListView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 07.09.2016.
 */
public abstract class BaseRecipePresenter<T extends IRecipeListView> extends BasePresenter<T> {
    protected RecipeLoaderTask task;

    @Override
    protected Class getDataManagerClass() {
        return RecipeListDataManager.class;
    }

    public void loadData(){
        if (task == null || task != null && task.getStatus() != AsyncTask.Status.RUNNING) {
            task = new RecipeLoaderTask(new IItemsLoadedListener<RecipeItem>() {
                @Override
                public void onItemsLoaded(ArrayList<RecipeItem> items) {
                    view().onItemsLoaded(items);
                }
            }, dataManager());
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params());
        }
    }

    protected abstract T view();

    protected abstract com.raizlabs.android.dbflow.sql.language.Condition.In params();

    protected RecipeListDataManager dataManager(){
        return (RecipeListDataManager)mDataManager;
    }
}
