package ru.maslov.ejuicecalendar.ui.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.core.EJuiceApplication;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors;

/**
 * Created by Администратор on 07.09.2016.
 */
public class RecipeFlavorsLayoutGenerator {

    public static void generateFlavorsLayout(Context context, ViewGroup parentLayout, ArrayList<RecipeFlavors> items){
        parentLayout.removeAllViews();
        for (RecipeFlavors item : items){
            View flavorView = LayoutInflater.from(context).inflate(R.layout.recipe_flavor_item, null);
            TextView flavorName = (TextView) flavorView.findViewById(R.id.recipeFlavorName);
            if (item.getFlavor().getIsInStock() == 1){
                flavorName.setTextColor(EJuiceApplication.getInstance().getResources().getColor(R.color.material_green_700));
            }
            flavorName.setText(item.getFlavor().getName());
            TextView flavorVolume = (TextView) flavorView.findViewById(R.id.recipeFlavorVolume);
            flavorVolume.setText(String.valueOf(item.getVolume()) + "%");
            parentLayout.addView(flavorView);
        }
    }
}
