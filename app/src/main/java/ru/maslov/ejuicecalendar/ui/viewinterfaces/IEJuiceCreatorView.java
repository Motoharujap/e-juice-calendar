package ru.maslov.ejuicecalendar.ui.viewinterfaces;

import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.sandbox.IView;

/**
 * Created by Администратор on 23.07.2016.
 */
public interface IEJuiceCreatorView extends IView {
    void fillViewWithJuiceData(Juice juice);
}
