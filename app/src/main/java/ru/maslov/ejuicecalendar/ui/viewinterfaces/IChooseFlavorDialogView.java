package ru.maslov.ejuicecalendar.ui.viewinterfaces;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.sandbox.IView;

/**
 * Created by Администратор on 30.09.2016.
 */

public interface IChooseFlavorDialogView extends IView {
    void onItemsLoaded(ArrayList<Flavor> items);
}
