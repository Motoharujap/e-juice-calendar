package ru.maslov.ejuicecalendar.ui.viewinterfaces;

import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;

/**
 * Created by Администратор on 17.08.2016.
 */
public interface IRecipeListFragmentView extends IRecipeListView {
    void onDataLoadingStarted();
    void onRecipesFiltered(ArrayList<RecipeItem> items);
    void onStartCreateRecipeFragment(@NonNull Bundle args);
}
