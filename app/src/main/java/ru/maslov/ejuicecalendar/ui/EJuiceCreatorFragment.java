package ru.maslov.ejuicecalendar.ui;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.util.Date;

import butterknife.Bind;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.event_bus.JuiceEvent;
import ru.maslov.ejuicecalendar.logic.JuiceParams;
import ru.maslov.ejuicecalendar.tools.DateTools;
import ru.maslov.ejuicecalendar.ui.dialogs.DialogManager;
import ru.maslov.ejuicecalendar.ui.dialogs.JuiceFlavorsVolumeDialog;
import ru.maslov.ejuicecalendar.ui.presenters.EJuiceCreatorPresenter;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IEJuiceCreatorView;
import ru.maslov.sandbox.BaseFragment;
import ru.maslov.sandbox.dialogs.Dialog;
import ru.maslov.sandbox.tools.TextFieldCheck;


/**
 * Created by Администратор on 23.07.2016.
 */
public class EJuiceCreatorFragment extends BaseFragment<EJuiceCreatorPresenter> implements IEJuiceCreatorView {
    @Bind(R.id.readyInValue) protected TextView mReadyInDateValue;
    @Bind(R.id.juiceNameValue)protected EditText mJuiceNameValue;
    @Bind(R.id.containerVolumeValue)protected EditText mContainerVolumeValue;
    @Bind(R.id.chooseRecipeBtn)protected LinearLayout mChooseRecipe;
    @Bind(R.id.recipeName)protected TextView mRecipeName;
    @Bind(R.id.calculateBtn) protected Button mCalculateButton;
    @Bind(R.id.datePickerLayout)protected LinearLayout mDatePicker;
    @Bind(R.id.fab) protected FloatingActionButton mFab;
    @Bind(R.id.pgVolumeValue)protected EditText pgValue;
    @Bind(R.id.vgVolumeValue)protected EditText vgValue;

    public static final String KEY_RECIPE_ID = "recipeId";
    public static final String KEY_JUICE_ID = "juiceId";

    private View.OnClickListener mChooseRecipeClickListener;
    private View.OnClickListener mChooseDateClickListener;
    private View.OnClickListener mSaveJuiceClickListener;


    public static EJuiceCreatorFragment getInstance(){
        return new EJuiceCreatorFragment();
    }

    public static EJuiceCreatorFragment getInstance(@NonNull Bundle args){
        EJuiceCreatorFragment fragment = new EJuiceCreatorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = super.onCreateView(inflater, container, savedInstanceState);
        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        parseArguments();
        initializeListeners();
    }

    private void parseArguments(){
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(KEY_RECIPE_ID)) {
                mPresenter.initJuiceByRecipeId(args.getLong(KEY_RECIPE_ID, -1));
            }
            if (args.containsKey(KEY_JUICE_ID)){
                mPresenter.initJuiceByJuiceId(args.getLong(KEY_JUICE_ID, -1));
            }
        }
    }

    private void initializeListeners(){
        mChooseRecipeClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoActivity(getActivity(), RecipeListActivity.class);
            }
        };
        mChooseRecipe.setOnClickListener(mChooseRecipeClickListener);

        mCalculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextFieldCheck.isTextFieldEmpty(mContainerVolumeValue)){
                    Toast.makeText(getActivity(), "Необходимо заполнить поле Объем тары!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mPresenter.recipe == null) {
                    Toast.makeText(getActivity(), "Необходимо выбрать рецепт!", Toast.LENGTH_SHORT).show();
                    return;
                }

                JuiceParams params = new JuiceParams();
                params.recipeId = mPresenter.recipe.getId();
                params.containerVolume = Double.parseDouble(mContainerVolumeValue.getText().toString());
                if (!TextFieldCheck.isTextFieldEmpty(pgValue)) {
                    params.pg = Double.parseDouble(pgValue.getText().toString().trim());
                }
                if (!TextFieldCheck.isTextFieldEmpty(vgValue)) {
                    params.vg = Double.parseDouble(vgValue.getText().toString().trim());
                }
                Bundle args = new Bundle();
                args.putSerializable(JuiceFlavorsVolumeDialog.KEY_JUICE_PARAMS, params);
                args.putString(JuiceFlavorsVolumeDialog.KEY_CAPTION,
                        String.format(getString(R.string.calculated_flavor_dialog_caption), Double.parseDouble(mContainerVolumeValue.getText().toString())));
                JuiceFlavorsVolumeDialog dialog = JuiceFlavorsVolumeDialog.getInstance(args);
                dialog.show(getFragmentManager(), "dialog");
            }
        });

        mChooseDateClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Date juiceDate = new Date(year - 1900, monthOfYear, dayOfMonth);
                        mReadyInDateValue.setText(DateTools.dateToString(juiceDate, DateTools.JUST_DATE));
                        mPresenter.saveJuiceReadyDate(juiceDate);
                    }
                };
                if (mPresenter.recipe == null) {
                    DialogManager.datePickerDialog(getActivity(), dateSetListener).show();
                } else {
                    DialogManager.datePickerDialog(getActivity(), dateSetListener, mPresenter.calculateJuiceReadyDate()).show();
                }
            }
        };
        mDatePicker.setOnClickListener(mChooseDateClickListener);

        mSaveJuiceClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFieldsNotEmpty()) {
                    mPresenter.saveJuiceName(mJuiceNameValue.getText().toString());
                    if (!TextFieldCheck.isTextFieldEmpty(mContainerVolumeValue)) {
                        mPresenter.saveJuiceContainerVolume(Double.valueOf(mContainerVolumeValue.getText().toString()));
                    }
                    if (!TextFieldCheck.isTextFieldEmpty(pgValue)){
                        mPresenter.savePGValue(Double.parseDouble(pgValue.getText().toString().trim()));
                    }
                    if (!TextFieldCheck.isTextFieldEmpty(vgValue)){
                        mPresenter.saveVGValue(Double.parseDouble(vgValue.getText().toString().trim()));
                    }
                    mPresenter.saveJuice();
                    EventBus.getDefault().post(new JuiceEvent(JuiceEvent.TYPE_REFRESH, null));
                    gotoActivity(getActivity(), StartActivity.class);
                }
            }
        };
        mFab.setOnClickListener(mSaveJuiceClickListener);
    }

    private boolean checkFieldsNotEmpty(){
        String date = mReadyInDateValue.getText().toString();
        StringBuffer errorMessage = new StringBuffer();
        if (TextFieldCheck.isTextFieldEmpty(mJuiceNameValue)){
            errorMessage.append(getString(R.string.juice_name));
            errorMessage.append(" ");
        }
        if (date.equals(getString(R.string.value_not_defined))){
            errorMessage.append(getString(R.string.juice_date));
        }
        if (!errorMessage.toString().equals("")){
            DialogManager.confirmDialog(getString(R.string.field_is_empty_caption), String.format(getString(R.string.field_is_empty_error), errorMessage.toString()),
                    Dialog.DIALOG_CONFIRM, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                        }
                    },
                    null).show(getFragmentManager(), "dialog");
            return false;
        }
        return true;
    }

    @Override
    protected EJuiceCreatorPresenter getPresenter() {
        if (mPresenter == null){
            mPresenter = new EJuiceCreatorPresenter();
        }
        return mPresenter;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.juice_creator_fragment;
    }

    @Override
    public void fillViewWithJuiceData(Juice juice) {
        if (!"".equals(juice.getRecipe().getRecipeName())) {
            mRecipeName.setText(juice.getRecipe().getRecipeName());
        }
        if (!"".equals(juice.getJuiceName()) && juice.getJuiceName() != null) {
            mJuiceNameValue.setText(juice.getJuiceName());
        //set juice name equal to recipe name
        } else if (!"".equals(juice.getRecipe().getRecipeName()) && juice.getRecipe().getRecipeName() != null){
            mJuiceNameValue.setText(juice.getRecipe().getRecipeName());
        }
        if (juice.getReadyDate() != null) {
            mReadyInDateValue.setText(DateTools.dateToString(juice.getReadyDate(), DateTools.JUST_DATE));
        }
        if (juice.getPgVolume() != 0d) {
            pgValue.setText(String.valueOf(juice.getPgVolume()));
        }
        if (juice.getVgVolume() != 0d) {
            vgValue.setText(String.valueOf(juice.getVgVolume()));
        }
        if (juice.getContainerVolume() != 0d) {
            mContainerVolumeValue.setText(String.valueOf(juice.getContainerVolume()));
        }
    }
}
