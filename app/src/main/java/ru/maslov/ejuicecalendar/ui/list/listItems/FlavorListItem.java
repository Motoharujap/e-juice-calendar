package ru.maslov.ejuicecalendar.ui.list.listItems;

import ru.maslov.ejuicecalendar.database.model.Flavor;

/**
 * Created by Администратор on 04.09.2016.
 */
public class FlavorListItem {
    public Flavor flavor;
    public long recipesCount;
    public double averageVolume;


    public FlavorListItem(Flavor flavor) {
        this.flavor = flavor;
    }

    @Override
    public boolean equals(Object o) {
        FlavorListItem listItem = (FlavorListItem) o;
        return listItem.flavor.getName().equals(flavor.getName());
    }
}
