package ru.maslov.ejuicecalendar.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import at.grabner.circleprogress.CircleProgressView;
import at.grabner.circleprogress.TextMode;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.tools.DateTools;
import ru.maslov.ejuicecalendar.ui.presenters.JuiceInfoDialogPresenter;
import ru.maslov.sandbox.dialogs.BaseDialogFragment;

/**
 * Created by Администратор on 28.07.2016.
 */
public class JuiceInfoDialog extends BaseDialogFragment<JuiceInfoDialogPresenter> {
    public static final String KEY_JUICE_ID = "juice_id";

    private long mJuiceId = -1;
    public static JuiceInfoDialog getInstance(Bundle args){
        JuiceInfoDialog dialog = new JuiceInfoDialog();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        parseArguments(getArguments());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(getLayoutResId(), null);
        if (view != null) {
            TextView juiceName = (TextView) view.findViewById(R.id.juiceName);
            TextView date = (TextView) view.findViewById(R.id.juiceDateValue);
            TextView recipe = (TextView) view.findViewById(R.id.recipeValue);
            TextView container = (TextView) view.findViewById(R.id.containerVolumeValue);

            final TextView ratingTxt = (TextView) view.findViewById(R.id.rating);
            RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
            CircleProgressView daysBeforeReady = (CircleProgressView) view.findViewById(R.id.circleView);
            LinearLayout daysLeftContainer = (LinearLayout) view.findViewById(R.id.daysLeftContainer);
            RelativeLayout recipeInfoHolder = (RelativeLayout) view.findViewById(R.id.recipe);

            final LinearLayout flavorDataContainer = (LinearLayout) view.findViewById(R.id.recipeFlavorsLayout);
            recipeInfoHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (flavorDataContainer.getVisibility() == View.GONE){
                        flavorDataContainer.setVisibility(View.VISIBLE);
                    } else {
                        flavorDataContainer.setVisibility(View.GONE);
                    }
                }
            });

            if (mJuiceId != -1) {
                mPresenter.initData(mJuiceId);
                juiceName.setText(mPresenter.juice.getJuiceName());
                date.setText(DateTools.dateToString(mPresenter.juice.getReadyDate(), DateTools.JUST_DATE));
                recipe.setText(mPresenter.getJuiceRecipeName());
                container.setText(String.valueOf(mPresenter.juice.getContainerVolume()));
                float initialValue = DateTools.daysBetween(new Date(System.currentTimeMillis()), mPresenter.juice.getReadyDate());
                float maxValue = DateTools.daysBetween(mPresenter.juice.getCreationDate(), mPresenter.juice.getReadyDate());
                daysBeforeReady.setTextMode(TextMode.VALUE);
                daysBeforeReady.setValueAnimated(initialValue);
                daysBeforeReady.setMaxValue(maxValue);

                ratingTxt.setText(ratingBarCaption(mPresenter.getRecipeRating()));
                ratingBar.setRating(mPresenter.getRecipeRating());

                ratingBar.setEnabled(isRatingBarEnabled());
                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        mPresenter.saveRating(rating);
                        ratingTxt.setText(ratingBarCaption(rating));
                        Toast.makeText(getActivity(), getString(R.string.rating_changed_message), Toast.LENGTH_SHORT).show();
                    }
                });
                fillFlavorViewContainer(flavorDataContainer);
                daysLeftContainer.setVisibility(isDaysLeftContainerVisible() ? View.VISIBLE : View.GONE);
            }
            builder.setView(view);
        }
        return builder.create();
    }

    private void parseArguments(Bundle args){
        if (args.containsKey(KEY_JUICE_ID)){
            mJuiceId = args.getLong(KEY_JUICE_ID);
        }
    }

    private void fillFlavorViewContainer(LinearLayout container){
        container.removeAllViews();
        ArrayList<View> childViews = JuiceDataProvider.getFlavorDataViews(getActivity(), mPresenter, mPresenter.params);
        for (View view : childViews){
            container.addView(view);
        }
    }

    @Override
    protected JuiceInfoDialogPresenter getPresenter() {
        if (mPresenter == null){
            mPresenter = new JuiceInfoDialogPresenter();
        }
        return mPresenter;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.juice_info_dialog_layout;
    }

    protected boolean isRatingBarEnabled(){
        return false;
    }

    protected String ratingNotDefinedCaption(){
        return getString(R.string.rating_not_determined);
    }

    protected boolean isDaysLeftContainerVisible(){
        return true;
    }

    protected String ratingBarCaption(float rating){
        if (rating == 0f){
            return ratingNotDefinedCaption();
        } else {
            return getString(R.string.rating);
        }
    }
}
