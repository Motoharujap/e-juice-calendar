package ru.maslov.ejuicecalendar.ui.list.listItems;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors;

/**
 * Created by Администратор on 17.08.2016.
 */
public class RecipeItem {
    public Recipe recipe;
    public ArrayList<RecipeFlavors> flavorItems = new ArrayList<>();

    public RecipeItem(Recipe recipe, ArrayList<RecipeFlavors> flavorItems) {
        this.recipe = recipe;
        this.flavorItems = flavorItems;
    }

    public boolean matchesSearchString(CharSequence sequence){
        for (RecipeFlavors item : flavorItems){
            if (item.getFlavor().getName().toLowerCase().contains(sequence.toString().toLowerCase())){
                return true;
            }
        }
        if (recipe.getRecipeName().toLowerCase().contains(sequence.toString().toLowerCase())){
            return true;
        }
        return false;
    }
}
