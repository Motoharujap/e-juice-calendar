package ru.maslov.ejuicecalendar.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.ui.dialogs.DialogManager;
import ru.maslov.ejuicecalendar.ui.presenters.CreateFlavorPresenter;
import ru.maslov.sandbox.dialogs.BaseDialogFragment;
import ru.maslov.sandbox.dialogs.Dialog;
import ru.maslov.sandbox.tools.TextFieldCheck;

/**
 * Created by Администратор on 09.09.2016.
 */
public class CreateFlavorFragment extends BaseDialogFragment<CreateFlavorPresenter> {
    @Bind(R.id.flavorNameInp)protected EditText flavorNameInp;
    @Bind(R.id.inStockCheck)protected CheckBox inStockCheck;
    @Bind(R.id.saveFlavor)protected Button saveFlavorBtn;
    @Bind(R.id.cancel)protected Button cancelBtn;
    @Bind(R.id.inStockLayout)protected RelativeLayout inStockLayout;

    private int mode = 0;
    private String flavorName;

    public static final int MODE_CREATE_FLAVOR = 0;
    public static final int MODE_EDIT_FLAVOR = 1;

    public static final String KEY_MODE = "mode";
    public static final String KEY_FLAVOR_NAME = "flavor_name";
    public static CreateFlavorFragment getInstance(@Nullable Bundle args){
        CreateFlavorFragment createFlavorFragment = new CreateFlavorFragment();
        if (args != null){
            createFlavorFragment.setArguments(args);
        }
        return createFlavorFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = LayoutInflater.from(getActivity()).inflate(getLayoutResId(), null);
        ButterKnife.bind(this, mainView);
        parseArguments();
        saveFlavorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextFieldCheck.isTextFieldEmpty(flavorNameInp)){
                    switch (mode){
                        case MODE_CREATE_FLAVOR:{
                            mPresenter.saveFlavor(flavorNameInp.getText().toString().trim(), inStockCheck.isChecked());
                            break;
                        }
                        case MODE_EDIT_FLAVOR:{
                            mPresenter.editFlavor(flavorName, flavorNameInp.getText().toString().trim());
                            break;
                        }
                    }
                    CreateFlavorFragment.this.dismiss();
                } else {
                    DialogManager.infoDialog(null, getString(R.string.flavor_name_empty_message), Dialog.DIALOG_MESSAGE).show(getFragmentManager(), "dialog");
                }
            }
        });
        if (mode == MODE_EDIT_FLAVOR){
            inStockLayout.setVisibility(View.GONE);
            flavorNameInp.setText(flavorName);
        }
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateFlavorFragment.this.dismiss();
            }
        });
        return mainView;
    }

    @Override
    public void onDestroyView(){
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    protected CreateFlavorPresenter getPresenter() {
        return new CreateFlavorPresenter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.create_flavor_dialog_layout;
    }

    private void parseArguments(){
        Bundle args = getArguments();
        if (args != null) {
            mode = args.getInt(KEY_MODE);
            flavorName = args.getString(KEY_FLAVOR_NAME);
        }
    }
}
