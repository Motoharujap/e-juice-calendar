package ru.maslov.ejuicecalendar.ui.dataManagers;

import com.raizlabs.android.dbflow.sql.language.Condition;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.data.RecipeCategoryInfo;
import ru.maslov.ejuicecalendar.database.model.RecipeCategory;

/**
 * Created by Администратор on 21.09.2016.
 */
public class RecipeCategoryDataManager extends BaseListDataManager<RecipeCategoryInfo> {

    @Override
    protected ArrayList<RecipeCategoryInfo> loadData(Condition.In condition) {
        ArrayList<RecipeCategory> initialData = JuiceDataProvider.getRecipeCategories();
        ArrayList<RecipeCategoryInfo> result = new ArrayList<>();
        for (RecipeCategory category : initialData){
            result.add(new RecipeCategoryInfo(category));
        }
        return result;
    }
}
