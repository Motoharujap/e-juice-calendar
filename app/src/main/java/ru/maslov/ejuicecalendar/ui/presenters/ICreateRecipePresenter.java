package ru.maslov.ejuicecalendar.ui.presenters;

import ru.maslov.ejuicecalendar.logic.RecipeFlavorsViewData;

/**
 * Created by Администратор on 29.09.2016.
 */

public interface ICreateRecipePresenter {
    void initializeRecipeFlavors(long recipeId);
    void addRecipeFlavorsToSave(RecipeFlavorsViewData recipeFlavors);
    void addRecipeFlavorsToDelete(RecipeFlavorsViewData recipeFlavors);
    void addRecipeFlavorsToUpdate(RecipeFlavorsViewData recipeFlavors);
    void saveRecipe(String newRecipeName, int newDecoctionPeriod, int categoryListId);
}
