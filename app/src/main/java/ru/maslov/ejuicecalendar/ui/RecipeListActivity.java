package ru.maslov.ejuicecalendar.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.core.CoreActivity;

/**
 * Created by Администратор on 17.08.2016.
 */
public class RecipeListActivity extends CoreActivity {
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_recipe_list;
    }
    @Override
    protected void showMainFragment() {
        startFragmentTransaction(RecipeCategoryFragment.getInstance(), false, false, R.id.fragmentContainer);
    }

    @Override
    protected int getMainFragmentResId() {
        return R.layout.recipe_list_fragment_layout;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(R.id.fragmentContainer);
        if (currentFragment != null){
            FragmentManager childFragmentManager = currentFragment.getChildFragmentManager();
            if (!childFragmentManager.popBackStackImmediate()){
                super.onBackPressed();
            }
        }
    }
}
