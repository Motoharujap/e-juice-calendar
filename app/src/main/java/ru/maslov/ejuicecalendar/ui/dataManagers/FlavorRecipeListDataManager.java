package ru.maslov.ejuicecalendar.ui.dataManagers;

/**
 * Created by Sergey Maslov on 12.09.2016.
 * I need this class because if I use RecipeListDataManager for both
 * RecipeListFragment and FlavorRecipeListFragment, on FRLF start
 * data manager does not reload it's items because it already exists
 * in GlobalDataManager
 */
public class FlavorRecipeListDataManager extends RecipeListDataManager {
}
