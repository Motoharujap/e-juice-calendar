package ru.maslov.ejuicecalendar.ui.presenters;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.FlavorInfo;
import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.ui.dataManagers.FlavorListDataManager;
import ru.maslov.ejuicecalendar.ui.list.listItems.FlavorListItem;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IFlavorListView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 03.09.2016.
 */
public class FlavorListFragmentPresenter extends BasePresenter<IFlavorListView> {
    private static final String TAG = FlavorListFragmentPresenter.class.getSimpleName();

    private FlavorLoader flavorLoader;

    @Override
    protected Class getDataManagerClass() {
        return FlavorListDataManager.class;
    }

    public void loadItems(){
        if (flavorLoader == null || flavorLoader != null && flavorLoader.getStatus() != AsyncTask.Status.RUNNING) {
            flavorLoader = new FlavorLoader();
            flavorLoader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    public void deleteFlavor(int flavorItemPosition){
        Flavor flavor = getItem(flavorItemPosition).flavor;
        flavor.delete();
    }

    public FlavorInfo getItem(int position){
        ArrayList<FlavorInfo> items = getDataManager().getObjects(null);
        return items.get(position);
    }

    public void setDataChanged(){
        getDataManager().setDataChanged();
    }

    @Override
    protected IFlavorListView view(){
        if (mView.get() == null){
            return new IFlavorListView() {
                @Override
                public void onItemsLoaded(ArrayList<FlavorListItem> items) {

                }

                @Override
                public String getString(int i) {
                    return null;
                }
            };
        }
        return mView.get();
    }

    private FlavorListDataManager getDataManager(){
        return (FlavorListDataManager) mDataManager;
    }

    class FlavorLoader extends AsyncTask<Void, Void, ArrayList<FlavorListItem>>{

        @Override
        protected ArrayList<FlavorListItem> doInBackground(Void... params) {
            ArrayList<FlavorListItem> items = new ArrayList<>();
            ArrayList<FlavorInfo> infoList = getDataManager().getObjects(null);
            for (FlavorInfo info : infoList){
                FlavorListItem newItem = new FlavorListItem(info.flavor);
                newItem.recipesCount = JuiceDataProvider.getFlavorsCountInRecipes(info.flavor.getId()).size();
                newItem.averageVolume = JuiceDataProvider.getFlavorVolumesAverage(info.flavor.getId());
                items.add(newItem);
            }
            Log.d(TAG, "loading items, size is " + String.valueOf(items.size()));
            return items;
        }

        @Override
        protected void onPostExecute(ArrayList<FlavorListItem> flavorListItems) {
            view().onItemsLoaded(flavorListItems);
        }
    }
}
