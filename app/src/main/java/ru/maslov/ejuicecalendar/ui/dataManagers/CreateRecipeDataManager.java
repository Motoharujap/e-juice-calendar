package ru.maslov.ejuicecalendar.ui.dataManagers;

import android.util.Log;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.database.model.RecipeCategory;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors;
import ru.maslov.ejuicecalendar.logic.RecipeFlavorsViewData;
import ru.maslov.sandbox.dataLayer.IDataManager;

/**
 * Created by Администратор on 19.08.2016.
 */
public class CreateRecipeDataManager implements IDataManager {
    private static final String TAG = CreateRecipeDataManager.class.getSimpleName();

    private ArrayList<RecipeFlavorsViewData> flavorsToSave = new ArrayList<>();
    private ArrayList<RecipeFlavorsViewData> flavorsToUpdate = new ArrayList<>();
    private ArrayList<RecipeFlavorsViewData> flavorsToDelete = new ArrayList<>();

    private ArrayList<RecipeFlavorsViewData> flavors = new ArrayList<>();

    private int editingMode;
    private Recipe recipe;
    private long categoryId;

    public void saveFlavorViewData(RecipeFlavorsViewData data){
        flavorsToSave.add(data);
    }

    public ArrayList<RecipeFlavorsViewData> getFlavorsToSave(){
        return flavorsToSave;
    }

    public void deleteFlavor(RecipeFlavorsViewData data){
        flavorsToSave.remove(data);
    }

    public void addRecipesFlavorsToUpdate(long recipeId){
        ArrayList<RecipeFlavorsViewData> recipeFlavorsViewData = new ArrayList<>();
        ArrayList<RecipeFlavors> recipeFlavors = JuiceDataProvider.getRecipeFlavorsForRecipe(recipeId);
        for (RecipeFlavors flavors : recipeFlavors){
            RecipeFlavorsViewData data = new RecipeFlavorsViewData();
            data.setFlavor(flavors.getFlavor());
            data.setRecipeFlavors(flavors);
            recipeFlavorsViewData.add(data);
        }
        flavorsToUpdate.addAll(recipeFlavorsViewData);
    }

    public void initRecipeFlavors(long recipeId){
        ArrayList<RecipeFlavorsViewData> recipeFlavorsViewData = new ArrayList<>();
        ArrayList<RecipeFlavors> recipeFlavors = JuiceDataProvider.getRecipeFlavorsForRecipe(recipeId);
        for (RecipeFlavors flavors : recipeFlavors){
            RecipeFlavorsViewData data = new RecipeFlavorsViewData();
            data.setFlavor(flavors.getFlavor());
            data.setRecipeFlavors(flavors);
            recipeFlavorsViewData.add(data);
        }
        flavorsToUpdate.addAll(recipeFlavorsViewData);
    }

    public void addRecipesFlavorsToSave(RecipeFlavorsViewData recipeFlavors){
        flavorsToSave.add(recipeFlavors);
    }

    public void initRecipe(long recipeId){
        recipe = JuiceDataProvider.getRecipeById(recipeId);
    }
    public int categoryListIdByRecipe(){
        return categoryListId(recipe.getCategoryId());
    }

    public int categoryListIdByCategory(){
        return categoryListId(categoryId);
    }

    private int categoryListId(long categoryId){
        ArrayList<RecipeCategory> categories = JuiceDataProvider.getRecipeCategories();
        for (RecipeCategory category : categories){
            if (category.getId() == categoryId){
                return categories.indexOf(category);
            }
        }
        Log.e(TAG, "Failed to initialize category list position");
        return 0;
    }

    public Recipe recipe(){
        return recipe != null ? recipe : Recipe.NULL;
    }

    public void addRecipesFlavorsToDelete(RecipeFlavorsViewData recipeFlavors){
        flavorsToDelete.add(recipeFlavors);
    }

    public void setEditMode(int editMode){
        editingMode = editMode;
    }

    public int editMode(){
        return editingMode;
    }

    public void setCategoryId(long categoryId){
        this.categoryId = categoryId;
    }

    public ArrayList<RecipeFlavorsViewData> getFlavorsToUpdate() {
        return flavorsToUpdate;
    }

    public ArrayList<RecipeFlavorsViewData> getFlavorsToDelete() {
        return flavorsToDelete;
    }

    public void saveRecipeFlavors(){
        for (RecipeFlavorsViewData toSave : flavorsToSave){
            toSave.save();
        }
        processUpdateAndDeleteFlavors();
    }

    public void saveRecipeFlavors(long recipeId){
        for (RecipeFlavorsViewData toSave : flavorsToSave){
            toSave.getRecipeFlavors().setRecipeId(recipeId);
            toSave.save();
        }
        processUpdateAndDeleteFlavors();
    }

    private void processUpdateAndDeleteFlavors(){
        for (RecipeFlavorsViewData toUpdate : flavorsToUpdate){
            toUpdate.update();
        }

        for (RecipeFlavorsViewData toDelete : flavorsToDelete){
            toDelete.delete();
        }
    }

    @Override
    public void onDestroy() {

    }
}
