package ru.maslov.ejuicecalendar.ui.dataManagers;

import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.database.model.Juice_Table;

/**
 * Created by Администратор on 02.09.2016.
 */
public class EJuiceHistoryDataManager extends EjuiceScheduleDataManager {
    @Override
    public ArrayList<Juice> startDataLoading() {
        if (mIsReloadingRequired) {
            mJuices = (ArrayList<Juice>) new Select().from(Juice.class).where(Condition.column(Juice_Table.isFinished.getNameAlias()).is(1)).queryList();
            mIsReloadingRequired = false;
        }
        return mJuices;
    }
}
