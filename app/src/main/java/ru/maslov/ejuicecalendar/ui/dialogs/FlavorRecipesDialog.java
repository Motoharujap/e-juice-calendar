package ru.maslov.ejuicecalendar.ui.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.ui.CreateEJuiceActivity;
import ru.maslov.ejuicecalendar.ui.EJuiceCreatorFragment;
import ru.maslov.ejuicecalendar.ui.list.adapters.FlavorRecipesAdapter;
import ru.maslov.ejuicecalendar.ui.list.adapters.OnItemChosenListener;
import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;
import ru.maslov.ejuicecalendar.ui.presenters.FlavorRecipesDialogPresenter;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IFlavorRecipesDialogView;
import ru.maslov.sandbox.dialogs.BaseDialogFragment;

/**
 * Created by Администратор on 07.09.2016.
 */
public class FlavorRecipesDialog extends BaseDialogFragment<FlavorRecipesDialogPresenter> implements IFlavorRecipesDialogView{
    private FlavorRecipesAdapter adapter;
    @Bind(R.id.recipesList)ListView recipesList;
    @Bind(R.id.title)TextView title;
    private long flavorId;

    public static final String KEY_FLAVOR_ID = "key_flavor_id";
    @Override
    protected FlavorRecipesDialogPresenter getPresenter() {
        return new FlavorRecipesDialogPresenter();
    }

    public static FlavorRecipesDialog getInstance(@NonNull Bundle args){
        FlavorRecipesDialog dialog = new FlavorRecipesDialog();
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(getLayoutResId(), container, false);
        ButterKnife.bind(this, mainView);
        parseArguments();
        adapter = new FlavorRecipesAdapter(getActivity());
        adapter.setOnJuiceCreateListener(new OnItemChosenListener<RecipeItem>() {
            @Override
            public void onItemChosen(RecipeItem item) {
                Intent intent  = new Intent(getActivity(), CreateEJuiceActivity.class);
                intent.putExtra(EJuiceCreatorFragment.KEY_RECIPE_ID, item.recipe.getId());
                startActivity(intent);
            }
        });
        title.setText(String.format(getString(R.string.flavor_recipes_dialog_caption_unformatted), mPresenter.getFlavor(flavorId).getName()));
        recipesList.setAdapter(adapter);
        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadData(flavorId);
    }

    @Override
    public void onDestroyView(){
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.flavor_recipes_dialog_layout;
    }

    @Override
    public void onItemsLoaded(ArrayList<RecipeItem> items) {
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

    private void parseArguments(){
        Bundle args = getArguments();
        flavorId = args.getLong(KEY_FLAVOR_ID);
    }
}
