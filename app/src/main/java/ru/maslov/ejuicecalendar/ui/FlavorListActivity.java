package ru.maslov.ejuicecalendar.ui;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.core.CoreActivity;

public class FlavorListActivity extends CoreActivity {


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_create_recipe;
    }

    @Override
    protected void showMainFragment() {
        startFragmentTransaction(FlavorListFragment.getInstance(), false, false, getMainFragmentResId());
    }

    @Override
    protected int getMainFragmentResId() {
        return R.id.fragmentContainer;
    }
}
