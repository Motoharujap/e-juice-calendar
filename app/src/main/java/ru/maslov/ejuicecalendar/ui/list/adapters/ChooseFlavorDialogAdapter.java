package ru.maslov.ejuicecalendar.ui.list.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.database.model.Flavor;

/**
 * Created by Администратор on 01.10.2016.
 */

public class ChooseFlavorDialogAdapter extends BaseAdapter {
    private ArrayList<Flavor> items = new ArrayList<>();
    private Context context;

    public ChooseFlavorDialogAdapter(Context context) {
        this.context = context;
    }

    public void setItems(ArrayList<Flavor> items){
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((Flavor)getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.choose_flavor_list_item, null);
            holder = new ViewHolder();
            holder.flavorName = (TextView) convertView.findViewById(R.id.flavorName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Flavor flavor = (Flavor) getItem(position);
        holder.flavorName.setText(flavor.getName());
        return convertView;
    }

    class ViewHolder {
        TextView flavorName;
    }
}
