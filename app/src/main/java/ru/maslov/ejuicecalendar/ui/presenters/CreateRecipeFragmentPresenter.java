package ru.maslov.ejuicecalendar.ui.presenters;

import android.util.Log;

import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.data.Query;
import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.database.model.Flavor_Table;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.database.model.RecipeCategory;

import ru.maslov.ejuicecalendar.logic.RecipeFlavorsViewData;
import ru.maslov.ejuicecalendar.ui.dataManagers.CreateRecipeDataManager;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.ICreateRecipeFragmentView;
import ru.maslov.sandbox.BasePresenter;
import ru.maslov.sandbox.eventBus.LeaveStateEvent;

/**
 * Created by Администратор on 19.08.2016.
 */
public class CreateRecipeFragmentPresenter extends BasePresenter<ICreateRecipeFragmentView> implements ICreateRecipePresenter{
    private static final String TAG = CreateRecipeFragmentPresenter.class.getSimpleName();

    private EditMode modeEdit;
    private EditMode modeCreateAllCategories;
    private EditMode modeCreateChosenCategory;

    public ArrayList<RecipeFlavorsViewData> getRecipeFlavorsExceptDelete(){
        ArrayList<RecipeFlavorsViewData> flavorsAll = new ArrayList<>();
        flavorsAll.addAll(dataManager().getFlavorsToSave());
        flavorsAll.addAll(dataManager().getFlavorsToUpdate());
        return flavorsAll;
    }

    public boolean isFlavorsFilled(){
        return getRecipeFlavorsExceptDelete().size() > 0;
    }

    public void initRecipe(long recipeId){
        dataManager().initRecipe(recipeId);
    }

    public void initRecipeFlavors(long recipeId){
        dataManager().initRecipeFlavors(recipeId);
    }

    public void setEditMode(int editMode){
        dataManager().setEditMode(editMode);
    }

    public EditMode editMode(){
        switch(dataManager().editMode()){
            case EditMode.MODE_EDIT_RECIPE:{
                return modeEdit;
            }
            case EditMode.MODE_CREATE_RECIPE_CHOSEN_CATEGORY:{
                return modeCreateChosenCategory;
            }
            case EditMode.MODE_CREATE_RECIPE_ALL_CATEGORIES:{
                return modeCreateAllCategories;
            }
            default:{
                Log.e(TAG, "CRITICAL ERROR: EditingMode is null");
                return new EditMode() {
                    @Override
                    public int categoryListPosition() {
                        return 0;
                    }

                    @Override
                    public void initFlavorViewData() {

                    }

                    @Override
                    public String recipeName() {
                        return "";
                    }

                    @Override
                    public int decoctionPeriodLength() {
                        return 0;
                    }

                    @Override
                    public void saveRecipe(Recipe recipe) {

                    }

                    @Override
                    public void saveRecipeFlavors() {
                    }

                    @Override
                    public void onFlavorNameChanged(RecipeFlavorsViewData data, String newFlavorName) {

                    }
                };
            }
        }
    }

    public String getFlavorName(RecipeFlavorsViewData data){
        Flavor flavor = data.getFlavor();
        if (!flavor.equals(Flavor.NULL)){
            return flavor.getName();
        }
        return view().getString(R.string.flavor_view_name);
    }

    public String onFlavorChosen(long flavorId){
        return JuiceDataProvider.getFlavor(new Query(Condition.column(Flavor_Table.id.getNameAlias()).is(flavorId))).getName();
    }

    public void setCategoryId(long categoryId){
        dataManager().setCategoryId(categoryId);
    }

    public ArrayList<String> categoriesNames(){
        ArrayList<String> names = new ArrayList<>();
        ArrayList<RecipeCategory> categories = JuiceDataProvider.getRecipeCategories();
        for (RecipeCategory category : categories){
            if (category.getId() != 0) {
                names.add(category.localizedName());
            }
        }
        return names;
    }

    public void initializeModes(){
        modeEdit = new EditMode() {
            @Override
            public int categoryListPosition() {
                return dataManager().categoryListIdByRecipe();
            }

            @Override
            public void initFlavorViewData() {
                dataManager().addRecipesFlavorsToUpdate(dataManager().recipe().getId());
            }

            @Override
            public String recipeName() {
                return dataManager().recipe().getRecipeName();
            }

            @Override
            public int decoctionPeriodLength() {
                return dataManager().recipe().getDecoctionPeriodLength();
            }

            @Override
            public void saveRecipe(Recipe recipe) {
                recipe.update();
            }

            @Override
            public void saveRecipeFlavors() {
                dataManager().saveRecipeFlavors(dataManager().recipe().getId());
            }

            @Override
            public void onFlavorNameChanged(RecipeFlavorsViewData data, String newFlavorName) {

            }
        };

        modeCreateAllCategories = new EditMode() {
            @Override
            public int categoryListPosition() {
                return 0;
            }

            @Override
            public void initFlavorViewData() {

            }

            @Override
            public String recipeName() {
                return "";
            }

            @Override
            public int decoctionPeriodLength() {
                return -1;
            }

            @Override
            public void saveRecipe(Recipe recipe) {
                recipe.save();
            }

            @Override
            public void saveRecipeFlavors() {
                dataManager().saveRecipeFlavors();
            }

            @Override
            public void onFlavorNameChanged(RecipeFlavorsViewData data, String newFlavorName) {

            }
        };

        modeCreateChosenCategory = new EditMode() {
            @Override
            public int categoryListPosition() {
                return dataManager().categoryListIdByCategory();
            }

            @Override
            public void initFlavorViewData() {

            }

            @Override
            public String recipeName() {
                return "";
            }

            @Override
            public int decoctionPeriodLength() {
                return -1;
            }

            @Override
            public void saveRecipe(Recipe recipe) {
                recipe.save();
            }

            @Override
            public void saveRecipeFlavors() {
                dataManager().saveRecipeFlavors();
            }

            @Override
            public void onFlavorNameChanged(RecipeFlavorsViewData data, String newFlavorName) {

            }
        };
    }
    @Override
    public void saveRecipe(String recipeName, int decoctionPeriod, int categoryListId){
        Recipe recipe = dataManager().recipe();
        recipe.setRecipeName(recipeName);
        recipe.setCategoryId(JuiceDataProvider.getRecipeCategories().get(categoryListId).getId());
        recipe.setDecoctionPeriodLength(decoctionPeriod);
        editMode().saveRecipe(recipe);
        editMode().saveRecipeFlavors();
    }

    public void leaveState(){
        EventBus.getDefault().post(new LeaveStateEvent(CreateRecipeFragmentPresenter.class.getSimpleName()));
    }


    public void updateRecipeFlavor(RecipeFlavorsViewData data, long newFlavorId){
        Flavor newFlavor = JuiceDataProvider.getFlavor(new Query(Condition.column(Flavor_Table.id.getNameAlias()).is(newFlavorId)));
        if (!newFlavor.equals(Flavor.NULL)){
            data.setFlavor(newFlavor);
        }
    }
    private CreateRecipeDataManager dataManager(){
        return (CreateRecipeDataManager)mDataManager;
    }

    public void onFlavorNameChanged(RecipeFlavorsViewData data, String newFlavorName){
        //check if flavor with such name already exists
        Flavor flavor = JuiceDataProvider.getFlavor(new Query(Condition.column(Flavor_Table.name.getNameAlias()).is(newFlavorName.trim())));
        if (flavor.equals(Flavor.NULL)){
            flavor = new Flavor(newFlavorName);
            data.setFlavor(flavor);
        } else {
            data.getRecipeFlavors().setFlavorId(flavor.getId());
        }
    }

    @Override
    protected Class getDataManagerClass() {
        return CreateRecipeDataManager.class;
    }

    @Override
    public void initializeRecipeFlavors(long recipeId) {

    }

    @Override
    public void addRecipeFlavorsToSave(RecipeFlavorsViewData recipeFlavors) {
        dataManager().addRecipesFlavorsToSave(recipeFlavors);
    }

    @Override
    public void addRecipeFlavorsToDelete(RecipeFlavorsViewData recipeFlavors) {
        dataManager().addRecipesFlavorsToDelete(recipeFlavors);
    }

    @Override
    public void addRecipeFlavorsToUpdate(RecipeFlavorsViewData recipeFlavors) {

    }

    protected ICreateRecipeFragmentView view(){
        if (mView == null || mView.get() == null){
            return new ICreateRecipeFragmentView() {
                @Override
                public String getString(int i) {
                    return "";
                }
            };
        }
        return mView.get();
    }

    public interface EditMode {
        int MODE_EDIT_RECIPE = 100;
        int MODE_CREATE_RECIPE_ALL_CATEGORIES = 101;
        int MODE_CREATE_RECIPE_CHOSEN_CATEGORY = 102;

        int categoryListPosition();
        void initFlavorViewData();
        String recipeName();
        int decoctionPeriodLength();
        void saveRecipe(Recipe recipe);
        void saveRecipeFlavors();
        void onFlavorNameChanged(RecipeFlavorsViewData data, String newFlavorName);
    }
}
