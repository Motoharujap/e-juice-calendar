package ru.maslov.ejuicecalendar.ui.viewinterfaces;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.RecipeCategoryInfo;
import ru.maslov.ejuicecalendar.database.model.RecipeCategory;
import ru.maslov.sandbox.IView;

/**
 * Created by Администратор on 21.09.2016.
 */
public interface IRecipeCategoryView extends IView {
    void onItemsLoaded(ArrayList<RecipeCategoryInfo> items);
}
