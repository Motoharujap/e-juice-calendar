package ru.maslov.ejuicecalendar.ui.presenters;

import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.event_bus.JuiceEvent;
import ru.maslov.ejuicecalendar.ui.dataManagers.EjuiceScheduleDataManager;
import ru.maslov.ejuicecalendar.ui.list.listItems.ScheduleItem;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IEjuiceScheduleView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 23.07.2016.
 */
public class EjuiceSchedulePresenter extends BasePresenter<IEjuiceScheduleView> {
    private ArrayList<ScheduleItem> mItems = new ArrayList<>();

    public EjuiceSchedulePresenter() {

    }

    public void loadData(){
        createListItems(getDataManager().startDataLoading());
    }

    private void createListItems(ArrayList<Juice> model){
        mItems.clear();
        for (Juice juice : model){
            ScheduleItem item = new ScheduleItem(
                    juice.getJuiceName(),
                    juice.getReadyDate()
            );
            mItems.add(item);
        }
        view().onItemsLoaded(mItems);
    }

    @Nullable
    public long getJuiceIdByPosition(int position){
        Juice juice = getDataManager().getJuiceByPosition(position);
        if (juice != null){
            return juice.getId();
        }
        return -1;
    }

    public void deleteJuice(int itemPosition){
        getDataManager().deleteItem(itemPosition);
        loadData();
        EventBus.getDefault().post(new JuiceEvent(JuiceEvent.TYPE_REFRESH, null));
    }

    @Override
    protected Class getDataManagerClass() {
        return EjuiceScheduleDataManager.class;
    }

    protected EjuiceScheduleDataManager getDataManager(){
        return (EjuiceScheduleDataManager)mDataManager;
    }

    protected IEjuiceScheduleView view(){
        if (mView.get() == null){
            return new IEjuiceScheduleView() {
                @Override
                public void onItemsLoaded(ArrayList<ScheduleItem> items) {

                }

                @Override
                public String getString(int i) {
                    return null;
                }
            };
        }
        return mView.get();
    }
}
