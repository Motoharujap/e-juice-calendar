package ru.maslov.ejuicecalendar.ui.list.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.Bind;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.ui.list.RecipeFlavorsLayoutGenerator;
import ru.maslov.ejuicecalendar.ui.list.listItems.RecipeItem;
import ru.maslov.sandbox.lists.BaseRecyclerViewAdapter;

/**
 * Created by Администратор on 17.08.2016.
 */
public class RecipeListAdapter extends BaseRecyclerViewAdapter<RecipeItem> {

    private OnRecipeChosenClickListener onRecipeCreateListener;
    private OnRecipeChosenClickListener onRecipeDeleteListener;
    private OnMenuItemClickListener onMenuItemClickListener;

    public RecipeListAdapter(Context context) {
        super(context);
    }

    public void setOnRecipeCreateListener(OnRecipeChosenClickListener onRecipeCreateListener) {
        this.onRecipeCreateListener = onRecipeCreateListener;
    }

    public void setOnRecipeDeleteListener(OnRecipeChosenClickListener onRecipeDeleteListener) {
        this.onRecipeDeleteListener = onRecipeDeleteListener;
    }

    public void setOnMenuItemClickListener(OnMenuItemClickListener onMenuItemClickListener) {
        this.onMenuItemClickListener = onMenuItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View convertView = LayoutInflater.from(mContext).inflate(R.layout.recipe_list_item, parent, false);
        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RecipeItem item = (RecipeItem) getItem(position);
        final ViewHolder viewHolder = (ViewHolder)holder;
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.recipeFlavorsLayout.getVisibility() == View.GONE){
                    viewHolder.recipeFlavorsLayout.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.recipeFlavorsLayout.setVisibility(View.GONE);
                }
            }
        });

        viewHolder.createJuiceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecipeCreateListener.onRecipeChosen(position);
            }
        });
        viewHolder.overflowMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu menu = new PopupMenu(mContext, v);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        onMenuItemClickListener.onMenuItemSelected(item, position);
                        return true;
                    }
                });
                menu.inflate(R.menu.recipe_listitem_menu);
                menu.show();
            }
        });
        viewHolder.recipeName.setText(item.recipe.getRecipeName());
        viewHolder.recipeRating.setRating(item.recipe.getRating());
        RecipeFlavorsLayoutGenerator.generateFlavorsLayout(mContext, viewHolder.recipeFlavorsLayout, item.flavorItems);
    }



    class ViewHolder extends BaseViewHolder{
        @Bind(R.id.recipeName)TextView recipeName;
        @Bind(R.id.recipeRating)RatingBar recipeRating;
        @Bind(R.id.mainInfoLayout)RelativeLayout mainLayout;
        @Bind(R.id.recipeFlavorsLayout)LinearLayout recipeFlavorsLayout;
        @Bind(R.id.createJuiceBtn)Button createJuiceBtn;
        @Bind(R.id.overflowMenu)ImageButton overflowMenu;


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
