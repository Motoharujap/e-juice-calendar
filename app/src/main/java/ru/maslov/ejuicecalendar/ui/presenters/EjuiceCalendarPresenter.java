package ru.maslov.ejuicecalendar.ui.presenters;

import org.apache.commons.lang3.time.DateUtils;

import java.util.ArrayList;
import java.util.Date;

import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.ui.dataManagers.EJuiceCalendarDataManager;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IEjuiceCalendarView;
import ru.maslov.sandbox.BasePresenter;

/**
 * Created by Администратор on 23.07.2016.
 */
public class EjuiceCalendarPresenter extends BasePresenter<IEjuiceCalendarView> {

    public void loadData(){
        getDataManager().loadData();
    }

    public ArrayList<Juice> getJuicesAtDate(Date date){
        ArrayList<Juice> juices = getDataManager().getJuices();
        ArrayList<Juice> juicesAtDate = new ArrayList<>();
        for (Juice juice : juices){
            if (DateUtils.isSameDay(juice.getReadyDate(), date)){
                juicesAtDate.add(juice);
            }
        }
        return juicesAtDate;
    }

    public CharSequence[] getJuicesNames(){
        ArrayList<Juice> juices = getDataManager().getJuices();
        return getJuicesNames(juices);
    }

    public CharSequence[] getJuicesNames(ArrayList<Juice> juices){
        CharSequence[] names = new CharSequence[juices.size()];
        for (int i = 0; i < juices.size(); i++){
            names[i] = juices.get(i).getJuiceName() + " " + String.valueOf(juices.get(i).getContainerVolume()) +  "мл";
        }
        return names;
    }

    public void setDataChanged(){
        getDataManager().setDataChanged();
    }

    private EJuiceCalendarDataManager getDataManager(){
        return (EJuiceCalendarDataManager)mDataManager;
    }

    @Override
    protected Class getDataManagerClass() {
        return EJuiceCalendarDataManager.class;
    }
}
