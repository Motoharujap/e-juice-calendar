package ru.maslov.ejuicecalendar.ui.dataManagers;

import android.support.annotation.Nullable;

import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.sandbox.dataLayer.IDataManager;

/**
 * Created by Администратор on 30.07.2016.
 */
//use BaseListDataManager instead
@Deprecated
public abstract class BaseJuiceDataManager implements IDataManager{
    ArrayList<Juice> mJuices = new ArrayList<>();
    protected boolean mIsReloadingRequired = true;

    public ArrayList<Juice> startDataLoading(){
        if (mIsReloadingRequired) {
            mJuices = (ArrayList<Juice>) new Select().from(Juice.class).queryList();
            mIsReloadingRequired = false;
        }
        return mJuices;
    }

    public void setDataChanged(){
        mIsReloadingRequired = true;
    }

    @Nullable
    public Juice getJuiceByPosition(int position){
        Juice juice = mJuices.get(position);
        return juice;
    }

    public void deleteItem(int position){
        setDataChanged();
        Juice juice = mJuices.get(position);
        if (juice != null){
            juice.delete();
            mJuices.remove(position);
        }
    }
}
