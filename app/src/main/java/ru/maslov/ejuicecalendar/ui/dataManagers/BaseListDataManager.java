package ru.maslov.ejuicecalendar.ui.dataManagers;

import android.support.annotation.Nullable;

import com.raizlabs.android.dbflow.sql.language.Condition;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.database.IDatabaseModelRepresentation;
import ru.maslov.ejuicecalendar.event_bus.DataManagerReloadEvent;
import ru.maslov.ejuicecalendar.event_bus.RefreshEvent;
import ru.maslov.sandbox.dataLayer.IDataManager;

/**
 * Created by Администратор on 17.08.2016.
 */
public abstract class BaseListDataManager<T extends IDatabaseModelRepresentation> implements IDataManager {
    ArrayList<T> mObjects = new ArrayList<>();
    private boolean mIsReloadingRequired = true;

    public BaseListDataManager(){
        EventBus.getDefault().register(this);
    }

    public ArrayList<T> getObjects(Condition.In condition){
        if (mIsReloadingRequired) {
            mObjects = loadData(condition);
            mIsReloadingRequired = false;
        }
        return mObjects;
    }

    public void setDataChanged(){
        mIsReloadingRequired = true;
    }

    @Nullable
    public T getObjectByPosition(int position){
        T object = mObjects.get(position);
        return object;
    }

    public void deleteItem(int position){
        T object = mObjects.get(position);
        if (object != null){
            object.delete();
            mObjects.remove(position);
            setDataChanged();
        }
    }

    public void deleteItem(T item){
        if (mObjects.remove(item)){
            item.delete();
            setDataChanged();
        }
    }

    @Subscribe
    public void onRefresh(DataManagerReloadEvent refreshEvent){
        if (refreshEvent.type == RefreshEvent.TYPE_REFRESH){
            setDataChanged();
        }
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }

    protected abstract ArrayList<T> loadData(Condition.In condition);
}