package ru.maslov.ejuicecalendar.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.Bind;
import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.event_bus.FlavorEvent;
import ru.maslov.ejuicecalendar.ui.dialogs.FlavorRecipesDialog;
import ru.maslov.ejuicecalendar.ui.list.adapters.FlavorListAdapter;
import ru.maslov.ejuicecalendar.ui.list.adapters.OnItemClickListener;
import ru.maslov.ejuicecalendar.ui.list.adapters.OnMenuItemClickListener;
import ru.maslov.ejuicecalendar.ui.list.listItems.FlavorListItem;
import ru.maslov.ejuicecalendar.ui.presenters.FlavorListFragmentPresenter;
import ru.maslov.ejuicecalendar.ui.viewinterfaces.IFlavorListView;
import ru.maslov.sandbox.BaseFragment;

/**
 * Created by Администратор on 03.09.2016.
 */
public class FlavorListFragment extends BaseFragment<FlavorListFragmentPresenter> implements IFlavorListView {
    @Bind(R.id.listView)protected ListView flavorListView;
    @Bind(R.id.fab)protected FloatingActionButton fab;
    private FlavorListAdapter adapter;

    public static FlavorListFragment getInstance(){
        FlavorListFragment flavorListFragment = new FlavorListFragment();
        return flavorListFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = super.onCreateView(inflater, container, savedInstanceState);
        EventBus.getDefault().register(this);
        setHasOptionsMenu(true);
        adapter = new FlavorListAdapter(getActivity());
        adapter.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            @Override
            public void onMenuItemSelected(MenuItem item, int itemPosition) {
                switch (item.getItemId()){
                    case R.id.deleteFlavor:{
                        mPresenter.deleteFlavor(itemPosition);
                        break;
                    }
                    case R.id.editFlavor:{
                        Bundle args = new Bundle();
                        args.putInt(CreateFlavorFragment.KEY_MODE, CreateFlavorFragment.MODE_EDIT_FLAVOR);
                        args.putString(CreateFlavorFragment.KEY_FLAVOR_NAME, mPresenter.getItem(itemPosition).flavor.getName());
                        CreateFlavorFragment createFlavorFragment = CreateFlavorFragment.getInstance(args);
                        createFlavorFragment.show(getFragmentManager(), "dialog");
                        break;
                    }
                }
            }
        });
        flavorListView.setAdapter(adapter);
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                FlavorListItem item = (FlavorListItem) adapter.getItem(position);
                Bundle args = new Bundle();
                args.putLong(FlavorRecipesDialog.KEY_FLAVOR_ID, item.flavor.getId());
                FlavorRecipesDialog dialog = FlavorRecipesDialog.getInstance(args);
                dialog.show(getFragmentManager(), "dialog");
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateFlavorFragment createFlavorFragment = CreateFlavorFragment.getInstance(null);
                createFlavorFragment.show(getFragmentManager(), "dialog");
            }
        });
        mPresenter.loadItems();
        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadItems();
    }

    @Subscribe
    public void onFlavorEvent(FlavorEvent event){
        switch (event.type){
            case FlavorEvent.TYPE_REFRESH:{
                mPresenter.setDataChanged();
                mPresenter.loadItems();
                break;
            }
        }
    }

    @Override
    protected FlavorListFragmentPresenter getPresenter() {
        return new FlavorListFragmentPresenter();
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.flavor_list_fragment_layout;
    }

    @Override
    public void onItemsLoaded(ArrayList<FlavorListItem> items) {
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }
}
