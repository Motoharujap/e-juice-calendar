package ru.maslov.ejuicecalendar.network.rss.connectivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.core.EJuiceApplication;

/**
 * Created by Администратор on 21.08.2016.
 */
public class ConnectionMonitor {

    public static boolean isConnectedToNet(){
        ConnectivityManager manager = (ConnectivityManager) EJuiceApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo == null){
            return false;
        }
        return networkInfo.isConnected();
    }

    public static boolean checkInternetConnection(CoordinatorLayout layout){
        if (!isConnectedToNet()) {
            Snackbar.make(layout, R.string.connection_error, Snackbar.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}
