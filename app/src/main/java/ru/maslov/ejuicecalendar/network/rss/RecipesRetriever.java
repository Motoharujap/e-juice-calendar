package ru.maslov.ejuicecalendar.network.rss;

import android.database.sqlite.SQLiteConstraintException;
import android.util.Log;

import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.database.model.Flavor_Table;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors;

/**
 * Created by Администратор on 15.08.2016.
 */
public class RecipesRetriever {
    private ArrayList<RecipeFlavors> recipeFlavors = new ArrayList<>();
    private ArrayList<Flavor> flavors = new ArrayList<>();
    private ArrayList<Recipe> recipes = new ArrayList<>();

    private static final int ID_AROMA_NAME = 1;
    private static final int ID_AROMA_VOLUME = 2;

    private static final String TAG = RecipesRetriever.class.getSimpleName();

    private final String URL = "http://vapebomb.ru/blog/rss/";

    public void run(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getRecipes();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private void getRecipes() throws IOException {
        Document doc = null;
        try {
            doc = Jsoup.connect(URL).get();
        } catch (UnknownHostException uhe){
            return;
        }
        Elements items = getItemElements(doc);
        //each element represents a single recipe
        for (Element element : items){
            Log.i(TAG, element.select("title").text());
            //create a recipe
            Recipe recipe = new Recipe(element.select("title").text());

            //a workaround to replace &gt with > etc.
            String description = element.select("description").text();
            Document descriptionDoc = Jsoup.parse(description);
            int decoctionPeriod = getDecoctionPeriod(descriptionDoc);
            recipe.setDecoctionPeriodLength(decoctionPeriod);
            try {
                recipe.save();
            } catch (SQLiteConstraintException sql){
                continue;
            }
            //digging down to recipes aromas
            Elements html = descriptionDoc.select("html");
            Elements body = html.select("body");
            Elements table = body.select("table.reciept");
            //tbody is an element that contains all aromas included in the recipe
            Element tbody = table.get(0).child(0);
            //iterate through each aroma element in table and parse them
            parseAromaTable(tbody, recipe.getId());
        }
    }

    private void parseAromaTable(Element aromaTable, long recipeId){
        //each child is an aroma
        ArrayList<Element> tableChildren = aromaTable.children();
        for (int i = 0; i < tableChildren.size(); i++){
            Element child = tableChildren.get(i);
            if (child.children().size() == 0 ||
                i == 0/*this is a table header*/){
                continue;
            }
            ArrayList<Element> allChildElements = child.getAllElements();
            Element aromaProps = allChildElements.get(0);
            String aromaName = null;
            double aromaVolume = 0;
            for (int j = 1; j < aromaProps.children().size(); j++){
                Element aromaProp = aromaProps.children().get(j);
                TextNode aromaNode = null;
                try {
                    switch (j) {
                        case ID_AROMA_NAME: {
                            aromaNode = (TextNode) aromaProp.children().get(0).childNode(0);
                            //a very very very dirty hack
                            if (!aromaNode.text().trim().equals("Пропиленгликоль") &&
                                    !aromaNode.text().trim().equals("Глицерин")){
                                aromaName = aromaNode.text();
                            }
                            break;
                        }
                        case ID_AROMA_VOLUME: {
                            aromaNode = (TextNode) aromaProp.childNode(0);
                            String aromaVolumeText = aromaNode.text();
                            List<String> volumeValues = Arrays.asList(aromaVolumeText.split(" "));
                            String volumePercentageText = volumeValues.get(1);
                            String volumePercentageUnformatted = volumePercentageText.replaceAll("[^\\,‌​0123456789]", "");
                            String volumePercentageFormatted = volumePercentageUnformatted.replaceAll("\\,", ".");
                            aromaVolume = Double.parseDouble(volumePercentageFormatted);
                        }
                    }
                } catch (ClassCastException cce) {

                } catch (IndexOutOfBoundsException ie) {

                }
            }
            if (aromaName != null && aromaVolume != 0){
                Flavor flavor = new Select().from(Flavor.class).where(Condition.column(Flavor_Table.name.getNameAlias()).is(aromaName)).querySingle();
                if (flavor == null){
                    flavor = new Flavor(aromaName);
                    flavor.save();
                }
                RecipeFlavors recipeFlavors = new RecipeFlavors(recipeId, flavor.getId(), aromaVolume);
                recipeFlavors.save();
            }
        }
    }

    private int getDecoctionPeriod(Document doc){
        int decoctionPeriod = 0;
        try {
            Element html = doc.child(0);
            Element body = html.child(1);
            Element table = body.child(1);
            Element tbody = table.child(0);
            Element content = tbody.child(0);
            Element description = content.child(1);
            Element info = description.child(2);
            Element info2 = info.child(0);
            Element info3 = info2.child(2);
            Element decoctionInfo = info3.child(0);
            TextNode decoctionTextNode = (TextNode) decoctionInfo.childNode(0);
            String decoctionText = decoctionTextNode.text();
            List<String> list = Arrays.asList(decoctionText.split(" "));

            String decoctionPeriodTime = list.get(list.size() - 1);
            String decoctionPeriodInt = list.get(list.size() - 2);
            decoctionPeriod = getDecoctionPeriodInDays(decoctionPeriodTime, decoctionPeriodInt);
        } catch (IndexOutOfBoundsException iob) {

        }
        return decoctionPeriod;
    }

    private int getDecoctionPeriodInDays(String time, String intPeriod){
        int period = Integer.parseInt(intPeriod);
        if (time.contains("недел")){
            return period * 7;
        } else if (time.contains("дней") || time.contains("дня") || time.contains("день")){
            return period;
        }
        return 0;
    }


    private Elements getItemElements(Document document){
        Elements rssTag = document.select("rss");
        Elements channel = rssTag.select("channel");
        Elements items = channel.select("item");
        return items;
    }
}
