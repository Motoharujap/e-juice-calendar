package ru.maslov.ejuicecalendar.tools;

/**
 * Created by Администратор on 20.08.2016.
 */
public class JuiceCalculator {
    //used to calculate vg or flavors volume
    public static double calculateComponentVolume(double overallVolume, double componentPercents){
            return overallVolume * componentPercents / 100;
    }

    public static double calculatePGVolume(double overallVolume, double flavorPercents, double pgPercents){
            return overallVolume * pgPercents / 100 - calculateComponentVolume(overallVolume, flavorPercents);
    }
}
