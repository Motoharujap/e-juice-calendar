package ru.maslov.ejuicecalendar.tools;

import android.support.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Администратор on 23.07.2016.
 */
public class DateTools {
    public static final String JUST_DATE = "dd.MM.yyyy";
    public static final int DAYS_IN_YEAR = 365;
    public static String dateToString(Date date, @Nullable String format){
        SimpleDateFormat dateFormat;
        if (format != null) {
            dateFormat = new SimpleDateFormat(format);
        } else {
            dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        }
        return dateFormat.format(date);
    }

    public static Float daysBetween(Date date1, Date date2){
        return new Float(( (date2.getTime() - date1.getTime()) / (1000 * 60 * 60 * 24)));
    }
}
