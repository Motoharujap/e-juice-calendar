package ru.maslov.ejuicecalendar.data;

import android.content.Context;
import android.database.CursorIndexOutOfBoundsException;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.raizlabs.android.dbflow.sql.language.BaseCondition;
import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.From;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.core.EJuiceApplication;
import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.database.model.Flavor_Table;
import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.database.model.Juice_Table;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.database.model.RecipeCategory;
import ru.maslov.ejuicecalendar.database.model.RecipeCategory_Table;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors_Table;
import ru.maslov.ejuicecalendar.database.model.Recipe_Table;
import ru.maslov.ejuicecalendar.logic.FlavorViewData;
import ru.maslov.ejuicecalendar.logic.JuiceParams;
import ru.maslov.ejuicecalendar.tools.JuiceCalculator;
import ru.maslov.ejuicecalendar.ui.IFlavorViewDataProvider;

/**
 * Created by Администратор on 20.08.2016.
 */
public class JuiceDataProvider {
    private static final String TAG = JuiceDataProvider.class.getSimpleName();

    public static ArrayList<Juice> getJuices(){
        return (ArrayList<Juice>) new Select().from(Juice.class).queryList();
    }

    public static ArrayList<Flavor> getFlavors(){
        return (ArrayList<Flavor>) new Select().from(Flavor.class).orderBy(Flavor_Table.name.getNameAlias(), true).queryList();
    }

    /**
        returns flavors that match specified {@link com.raizlabs.android.dbflow.sql.language.BaseCondition}
        it's important to know that you cannot use this method without specifying a BaseCondition.
        If you need to retrieve all flavors use {@link #getFlavors()} method instead
     */
    public static ArrayList<Flavor> getFlavors(Query query){
        return (ArrayList<Flavor>) new Select().from(Flavor.class)
                .where(query.condition)
                .orderBy(query.orderBy == null ? Flavor.defaultOrder() : query.orderBy)
                .queryList();
    }

    public static Flavor getFlavor(Query query){
        Flavor flavor =  new Select().from(Flavor.class)
                .where(query.condition)
                .querySingle();
        if (flavor == null){
            flavor = Flavor.NULL;
        }
        return flavor;
    }

    public static ArrayList<Recipe> getRecipes(){
        return (ArrayList<Recipe>) new Select().from(Recipe.class).orderBy(Recipe_Table.id.getNameAlias(), false).queryList();
    }

    public static ArrayList<Recipe> getRecipes(Condition.In condition){
        From<Recipe> recipes = new Select().from(Recipe.class);
        if (condition != null){
            return (ArrayList<Recipe>) recipes.where(condition).orderBy(Recipe_Table.id.getNameAlias(), false).queryList();
        }
        return (ArrayList<Recipe>) recipes.orderBy(Recipe_Table.id.getNameAlias(), false).queryList();
    }

    public static ArrayList<RecipeCategory> getRecipeCategories(){
        ArrayList<RecipeCategory> result = (ArrayList<RecipeCategory>) new Select().from(RecipeCategory.class).orderBy(RecipeCategory_Table.id.getNameAlias(), false).queryList();
        return result;
    }

    public static ArrayList<RecipeFlavors> getRecipeFlavors(){
        return (ArrayList<RecipeFlavors>) new Select().from(RecipeFlavors.class).queryList();
    }

    public static ArrayList<RecipeFlavors> getRecipeFlavorsForRecipe(long recipeId){
        return (ArrayList<RecipeFlavors>) new Select().from(RecipeFlavors.class).where(Condition.column(RecipeFlavors_Table.recipeId.getNameAlias()).is(recipeId)).queryList();
    }

    public static RecipeFlavors getRecipeFlavors(long recipeId, long flavorId){
        RecipeFlavors flavors = new Select().from(RecipeFlavors.class).where(Condition.column(RecipeFlavors_Table.recipeId.getNameAlias()).is(recipeId))
                .and(Condition.column(RecipeFlavors_Table.flavorId.getNameAlias()).is(flavorId)).querySingle();
        if (flavors == null){
            flavors = RecipeFlavors.NULL;
        }
        return flavors;
    }

    public static ArrayList<RecipeFlavors> getFlavorsCountInRecipes(long flavorId){
        ArrayList<RecipeFlavors> list = (ArrayList<RecipeFlavors>) new Select().from(RecipeFlavors.class).where(Condition.column(RecipeFlavors_Table.flavorId.getNameAlias()).is(flavorId)).queryList();
        return list;
    }

    public static ArrayList<FlavorViewData> flavorsForRecipe(long recipeId){
        ArrayList<FlavorViewData> resultList = new ArrayList<>();
        ArrayList<RecipeFlavors> recipeFlavors = JuiceDataProvider.getRecipeFlavorsForRecipe(recipeId);
        for (RecipeFlavors flavors : recipeFlavors){
            FlavorViewData flavorViewData = new FlavorViewData();
            flavorViewData.flavorId = flavors.getFlavorId();
            flavorViewData.flavorName = flavors.getFlavor().getName();
            flavorViewData.flavorVolumePercents = flavors.getVolume();
            resultList.add(flavorViewData);
        }
        return resultList;
    }

    public static double getFlavorVolumesAverage(long flavorId){
        double flavorsVolumeAverage = 0;
        try {
            ArrayList<RecipeFlavors> list = (ArrayList<RecipeFlavors>) new Select().from(RecipeFlavors.class).where(Condition.column(RecipeFlavors_Table.flavorId.getNameAlias()).is(flavorId)).queryList();
            double accumulatedVolume = 0d;
            for (RecipeFlavors flavors : list){
                accumulatedVolume += flavors.getVolume();
            }
            if (list.size() > 0) {
                flavorsVolumeAverage = accumulatedVolume / list.size();
            }
        } catch (CursorIndexOutOfBoundsException iob){
            Log.w(TAG, "flavor is not used in any recipe");
            return 0d;
        }
        return flavorsVolumeAverage;
    }

    @NonNull
    public static Juice getJuiceById(long juiceId){
        Juice juice = new Select().from(Juice.class).where(Condition.column(Juice_Table.id.getNameAlias()).is(juiceId)).querySingle();
        if (juice == null){
            juice = Juice.NULL;
        }
        return juice;
    }
    @NonNull
    public static Recipe getRecipeById(long recipeId){
        Recipe recipe = new Select().from(Recipe.class).where(Condition.column(Recipe_Table.id.getNameAlias()).is(recipeId)).querySingle();
        if (recipe == null){
            recipe = Recipe.NULL;
        }
        return recipe;
    }

    public static ArrayList<FlavorViewData> getFlavorViewData(JuiceParams params){
        ArrayList<FlavorViewData> flavorViewDataArrayList = new ArrayList<>();
        ArrayList<RecipeFlavors> recipeFlavors = getRecipeFlavorsForRecipe(params.recipeId);
        double overallFlavorPercentage = 0.0;
        for (RecipeFlavors flavors : recipeFlavors){
            FlavorViewData data = new FlavorViewData();
            double flavorVolumePercents = flavors.getVolume();
            overallFlavorPercentage += flavorVolumePercents;
            double volumeMilliliters = JuiceCalculator.calculateComponentVolume(params.containerVolume, flavorVolumePercents);
            Flavor flavor = flavors.getFlavor();
            String flavorName = flavor.getName();
            data.flavorId = flavor.getId();
            data.flavorName = flavorName;
            data.flavorVolumeMilliliters = volumeMilliliters;
            data.flavorVolumePercents = flavorVolumePercents;
            flavorViewDataArrayList.add(data);
        }

        FlavorViewData pg = new FlavorViewData();
        pg.flavorName = EJuiceApplication.getInstance().getString(R.string.PG);
        pg.flavorVolumeMilliliters = params.pg == 0d ? 0 : JuiceCalculator.calculatePGVolume(params.containerVolume, overallFlavorPercentage, params.pg);
        pg.flavorVolumePercents = params.pg;
        FlavorViewData vg = new FlavorViewData();
        vg.flavorName = EJuiceApplication.getInstance().getString(R.string.VG);
        vg.flavorVolumeMilliliters = params.vg == 0d ? 0 : JuiceCalculator.calculateComponentVolume(params.containerVolume, params.vg);
        vg.flavorVolumePercents = params.vg;

        flavorViewDataArrayList.add(pg);
        flavorViewDataArrayList.add(vg);

        return flavorViewDataArrayList;
    }

    public static ArrayList<View> getFlavorDataViews(Context context, IFlavorViewDataProvider provider, JuiceParams params){
        ArrayList<View> views = new ArrayList<>();
        ArrayList<FlavorViewData> viewData = provider.getFlavorViewData(params);
        for (FlavorViewData data : viewData){
            View flavorView = LayoutInflater.from(context).inflate(R.layout.recipe_flavor_item, null);
            TextView flavorNameTV = (TextView) flavorView.findViewById(R.id.recipeFlavorName);
            if (data.flavorId != 0l){
                Flavor flavor = getFlavor(new Query(Condition.column(Flavor_Table.id.getNameAlias()).is(data.flavorId)));
                if (flavor.getIsInStock() == 1) {
                    flavorNameTV.setTextColor(EJuiceApplication.getInstance().getResources().getColor(R.color.material_green_700));
                }
            }
            flavorNameTV.setText(data.flavorName);
            TextView flavorVolumeTv = (TextView) flavorView.findViewById(R.id.recipeFlavorVolume);
            flavorVolumeTv.setText(String.format(context.getString(R.string.flavor_volume), data.flavorVolumeMilliliters, data.flavorVolumePercents));
            views.add(flavorView);
        }
        return views;
    }
}
