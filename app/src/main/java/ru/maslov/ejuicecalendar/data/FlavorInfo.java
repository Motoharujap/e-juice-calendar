package ru.maslov.ejuicecalendar.data;

import ru.maslov.ejuicecalendar.database.IDatabaseModelRepresentation;
import ru.maslov.ejuicecalendar.database.model.Flavor;

/**
 * Created by Администратор on 04.09.2016.
 */
public class FlavorInfo implements IDatabaseModelRepresentation {
    public Flavor flavor;

    @Override
    public void update() {
        flavor.update();
    }

    @Override
    public void delete() {
        flavor.delete();
    }

    @Override
    public void save() {
        flavor.save();
    }

    public FlavorInfo(Flavor flavor){
        this.flavor = flavor;
    }
}
