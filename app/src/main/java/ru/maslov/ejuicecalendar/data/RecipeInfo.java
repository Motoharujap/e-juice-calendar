package ru.maslov.ejuicecalendar.data;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.database.IDatabaseModelRepresentation;
import ru.maslov.ejuicecalendar.database.model.Recipe;
import ru.maslov.ejuicecalendar.database.model.RecipeFlavors;

/**
 * Created by Администратор on 21.08.2016.
 */
public class RecipeInfo implements IDatabaseModelRepresentation{
    public Recipe recipe;
    public ArrayList<RecipeFlavors> recipeFlavors = new ArrayList<>();

    public RecipeInfo(Recipe recipe){
        this.recipe = recipe;
        recipeFlavors = JuiceDataProvider.getRecipeFlavorsForRecipe(recipe.getId());
        for (RecipeFlavors flavors : recipeFlavors){
            flavors.getFlavor();
        }
    }

    @Override
    public void update() {
        if (recipe != null){
            recipe.update();
        }
        for (RecipeFlavors flavors : recipeFlavors){
            flavors.update();
        }
    }

    @Override
    public void delete() {
        if (recipe != null){
            recipe.delete();
        }
        for (RecipeFlavors flavors : recipeFlavors){
            flavors.delete();
        }
    }

    @Override
    public void save() {
        if (recipe != null){
            recipe.save();
        }
        for (RecipeFlavors flavors : recipeFlavors){
            flavors.save();
        }
    }
}
