package ru.maslov.ejuicecalendar.data;

import android.support.annotation.NonNull;

import ru.maslov.ejuicecalendar.database.IDatabaseModelRepresentation;
import ru.maslov.ejuicecalendar.database.model.RecipeCategory;

/**
 * Created by Администратор on 21.09.2016.
 */
public class RecipeCategoryInfo implements IDatabaseModelRepresentation {
    private RecipeCategory recipeCategory;

    public RecipeCategoryInfo(@NonNull RecipeCategory recipeCategory) {
        this.recipeCategory = recipeCategory;
    }

    public RecipeCategory category(){
        return recipeCategory;
    }

    @Override
    public void update() {
        recipeCategory.update();
    }

    @Override
    public void delete() {
        recipeCategory.delete();
    }

    @Override
    public void save() {
        recipeCategory.save();
    }
}
