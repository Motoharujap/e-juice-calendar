package ru.maslov.ejuicecalendar.data;

import android.support.annotation.NonNull;

import com.raizlabs.android.dbflow.sql.language.BaseCondition;
import com.raizlabs.android.dbflow.sql.language.NameAlias;
import com.raizlabs.android.dbflow.sql.language.OrderBy;

import java.util.Collections;
import java.util.List;

/**
 * Created by Администратор on 25.09.2016.
 */

public class Query {
    public BaseCondition condition;
    public OrderBy orderBy;

    public Query(@NonNull BaseCondition condition, NameAlias orderByColumnName, boolean orderByDescending) {
        this.condition = condition;
        this.orderBy = OrderBy.fromNameAlias(orderByColumnName);
        this.orderBy = orderByDescending ? this.orderBy.descending() : this.orderBy.ascending();
    }

    public Query(@NonNull BaseCondition condition) {
        this.condition = condition;
    }
}
