package ru.maslov.ejuicecalendar.database.model;

import android.util.Log;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.database.JuiceDatabase;
import ru.maslov.ejuicecalendar.event_bus.RecipeEvent;

/**
 * Created by Администратор on 31.07.2016.
 */
@Table(database = JuiceDatabase.class)
public class Recipe extends BaseModel {
    private static final String TAG = Recipe.class.getSimpleName();

    @Column
    @PrimaryKey(autoincrement = true)
    private long id;
    @Column
    @Unique
    private String recipeName;
    @Column
    private float rating;
    @Column
    private int decoctionPeriodLength;

    @Column
    private long categoryId;

    private RecipeCategory category;

    public static final Recipe NULL = new Recipe(-1, "УДАЛЕН", 0f, 0);

    public Recipe() {
    }

    private Recipe(long id, String recipeName, float rating, int decoctionPeriodLength) {
        this.id = id;
        this.recipeName = recipeName;
        this.rating = rating;
        this.decoctionPeriodLength = decoctionPeriodLength;
    }

    public Recipe(String recipeName) {
        this.recipeName = recipeName;
    }

    public RecipeCategory category(){
        if (category == null){
            category = new Select().from(RecipeCategory.class).where(Condition.column(RecipeCategory_Table.id.getNameAlias()).is(this.categoryId)).querySingle();
        }
        return category;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public ArrayList<RecipeFlavors> getFlavors(){
        ArrayList<RecipeFlavors> flavors = (ArrayList<RecipeFlavors>) new Select().
                from(RecipeFlavors.class).
                where(Condition.column(RecipeFlavors_Table.recipeId.getNameAlias()).
                is(this.id)).queryList();
        return flavors;
    }

    public int getDecoctionPeriodLength() {
        return decoctionPeriodLength;
    }

    public void setDecoctionPeriodLength(int decoctionPeriodLength) {
        this.decoctionPeriodLength = decoctionPeriodLength;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public void save() {
        if (notNull()) {
            Log.d(TAG, "Saving recipe " + recipeName);
            super.save();
            EventBus.getDefault().post(new RecipeEvent(RecipeEvent.TYPE_REFRESH));
        }
    }

    @Override
    public void update() {
        if (notNull()) {
            super.update();
        }
    }

    private boolean notNull(){
        return id != -1;
    }

    @Override
    public boolean equals(Object o) {
        Recipe recipe = (Recipe) o;
        return recipe.getId() == this.id;
    }
}
