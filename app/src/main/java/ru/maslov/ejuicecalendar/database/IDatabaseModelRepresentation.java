package ru.maslov.ejuicecalendar.database;

/**
 * Created by Администратор on 21.08.2016.
 */
public interface IDatabaseModelRepresentation {
    void update();
    void delete();
    void save();
}
