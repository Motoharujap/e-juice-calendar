package ru.maslov.ejuicecalendar.database.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Locale;

import ru.maslov.ejuicecalendar.database.JuiceDatabase;

/**
 * Created by Администратор on 18.09.2016.
 */
@Table(database = JuiceDatabase.class)
public class RecipeCategory extends BaseModel {
    public static final int CATEGORY_ALL_RECIPES_ID = 0;

    @Column
    @PrimaryKey(autoincrement = true)
    private long id;

    @Column
    private String nameRu;

    @Column
    private String nameEn;

    @Column
    private int pictureResId;

    public int getPictureResId() {
        return pictureResId;
    }

    public void setPictureResId(int pictureResId) {
        this.pictureResId = pictureResId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String localizedName(){
        switch (Locale.getDefault().getDisplayLanguage()){
            case "English":{
                return nameEn;
            }
            case "русский":{
                return nameRu;
            }
            default:{
                return "";
            }
        }
    }
}
