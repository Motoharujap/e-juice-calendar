package ru.maslov.ejuicecalendar.database.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.structure.BaseModel;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.data.Query;
import ru.maslov.ejuicecalendar.database.JuiceDatabase;

/**
 * Created by Администратор on 31.07.2016.
 */
@Table(database = JuiceDatabase.class)
public class RecipeFlavors extends BaseModel{
    @Column
    @PrimaryKey(autoincrement = true)
    private long id;
    @Column
    private long recipeId;
    @Column
    private long flavorId;
    @Column
    private double volume;

    private Flavor flavor = Flavor.NULL;
    private Recipe recipe = Recipe.NULL;

    public static final RecipeFlavors NULL = new RecipeFlavors(-1, -1, 0d);

    public RecipeFlavors() {
    }

    public RecipeFlavors(long recipeId, long flavorId, double volume) {
        this.recipeId = recipeId;
        this.flavorId = flavorId;
        this.volume = volume;
    }

    public Flavor getFlavor(){
        if (Flavor.NULL.equals(flavor)) {
            flavor = JuiceDataProvider.getFlavor(new Query(Condition.column(Flavor_Table.id.getNameAlias()).is(this.flavorId)));
        }
        return flavor;
    }

    public Recipe recipe(){
        if (Recipe.NULL.equals(recipe)) {
            recipe = JuiceDataProvider.getRecipeById(recipeId);
        }
        return recipe;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setFlavor(Flavor flavor) {
        this.flavor = flavor;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public long getFlavorId() {
        return flavorId;
    }

    public void setFlavorId(long flavorId) {
        this.flavorId = flavorId;
    }

    public long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(long recipeId) {
        this.recipeId = recipeId;
    }

    @Override
    public void save() {
        if (notNull()) {
            super.save();
        }
    }

    @Override
    public void update() {
        if (notNull()) {
            super.update();
        }
    }

    private boolean notNull(){
        return recipeId != -1 && flavorId != -1;
    }
}
