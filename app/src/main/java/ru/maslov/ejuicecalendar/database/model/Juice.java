package ru.maslov.ejuicecalendar.database.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Date;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.database.JuiceDatabase;

/**
 * Created by Администратор on 24.07.2016.
 */
@Table(database = JuiceDatabase.class)
public class Juice extends BaseModel {
    @Column
    @PrimaryKey(autoincrement = true)
    private long id;
    @Column
    private String juiceName;
    @Column
    private Date creationDate;
    @Column
    private Date readyDate;
    @Column
    private long recipeId = 0;
    @Column
    private double containerVolume = 0.0;
    @Column
    private float rating = 0f;
    @Column
    private double pgVolume = 0.0;
    @Column
    private double vgVolume = 0.0;
    @Column
    private int isFinished = 0;

    private Recipe recipe;
    public static final Juice NULL = new Juice(-1, "УДАЛЕН", new Date(), new Date(), 0, 0d, 0f, 0d, 0d, 0);

    public Juice() {
    }

    public Juice(String juiceName, Date readyDate) {
        this.juiceName = juiceName;
        this.readyDate = readyDate;
    }

    private Juice(long id, String juiceName, Date creationDate, Date readyDate, long recipeId, double containerVolume, float rating, double pgVolume, double vgVolume, int isFinished) {
        this.id = id;
        this.juiceName = juiceName;
        this.creationDate = creationDate;
        this.readyDate = readyDate;
        this.recipeId = recipeId;
        this.containerVolume = containerVolume;
        this.rating = rating;
        this.pgVolume = pgVolume;
        this.vgVolume = vgVolume;
        this.isFinished = isFinished;
    }

    public Recipe getRecipe() {
        if (recipe == null){
            recipe = JuiceDataProvider.getRecipeById(recipeId);
        }
        return recipe;
    }

    public int getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(int isFinished) {
        this.isFinished = isFinished;
    }

    public boolean isFinished(){
        //safety!!!
        if (isFinished == 1){
            return true;
            //safety everywhere!!!
        } else if (isFinished == 0){
            return false;
        }
        return false;
    }

    public double getPgVolume() {
        return pgVolume;
    }

    public void setPgVolume(double pgVolume) {
        this.pgVolume = pgVolume;
    }

    public double getVgVolume() {
        return vgVolume;
    }

    public void setVgVolume(double vgVolume) {
        this.vgVolume = vgVolume;
    }

    public long getId(){
        return id;
    }

    public void setId(long id){
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getJuiceName() {
        return juiceName;
    }

    public void setJuiceName(String mJuiceName) {
        this.juiceName = mJuiceName;
    }

    public Date getReadyDate() {
        return readyDate;
    }

    public void setReadyDate(Date mReadyDate) {
        this.readyDate = mReadyDate;
    }

    public long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(long mRecipeId) {
        this.recipeId = mRecipeId;
    }

    public double getContainerVolume() {
        return containerVolume;
    }

    public void setContainerVolume(double mContainerVolume) {
        this.containerVolume = mContainerVolume;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float mRating) {
        this.rating = mRating;
    }

    @Override
    public void save() {
        if (notNull()) {
            super.save();
        }
    }

    @Override
    public void update() {
        if (notNull()) {
            super.update();
        }
    }

    private boolean notNull(){
        return id != -1;
    }

    @Override
    public boolean equals(Object o) {
        Juice juice = (Juice) o;
        return juice.getId() == this.id;
    }
}
