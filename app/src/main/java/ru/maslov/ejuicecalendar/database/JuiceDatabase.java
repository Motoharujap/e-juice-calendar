package ru.maslov.ejuicecalendar.database;

import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;

import ru.maslov.ejuicecalendar.database.model.Flavor;
import ru.maslov.ejuicecalendar.database.model.Recipe;

/**
 * Created by Администратор on 24.07.2016.
 */
@Database(name = JuiceDatabase.NAME, version = JuiceDatabase.VERSION)
public class JuiceDatabase {
    public static final String NAME = "JuiceDatabase";
    public static final int VERSION = 9;
}
