package ru.maslov.ejuicecalendar.database.model;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.OrderBy;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.raizlabs.android.dbflow.structure.BaseModel;

import org.greenrobot.eventbus.EventBus;

import ru.maslov.ejuicecalendar.data.Query;
import ru.maslov.ejuicecalendar.database.JuiceDatabase;
import ru.maslov.ejuicecalendar.event_bus.FlavorEvent;

/**
 * Created by Администратор on 31.07.2016.
 */
@Table(database = JuiceDatabase.class)
public class Flavor extends BaseModel {
    @Column
    @PrimaryKey(autoincrement = true)
    private long id;
    @Unique
    @Column
    private String name;
    @Column
    private int isInStock;
    @Column
    private long categoryId;

    private FlavorCategory category;

    public static final Flavor NULL = new Flavor(-1, "УДАЛЕН");

    public Flavor() {
    }

    private Flavor(long id, String name) {
        this.id = id;
        this.name = name;
        isInStock = 0;
    }

    public FlavorCategory category(){
        if (category == null){
            category = new Select().from(FlavorCategory.class).where(Condition.column(FlavorCategory_Table.id.getNameAlias()).is(this.id)).querySingle();
        }
        return category;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public int getIsInStock() {
        return isInStock;
    }

    public void setIsInStock(int isInStock) {
        this.isInStock = isInStock;
    }

    public Flavor(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void save() {
        if (notNull()) {
            super.save();
            EventBus.getDefault().post(new FlavorEvent(FlavorEvent.TYPE_REFRESH));
        }
    }

    public void silentUpdate(){
        super.update();
    }

    @Override
    public void update() {
        if (notNull()) {
            super.update();
            EventBus.getDefault().post(new FlavorEvent(FlavorEvent.TYPE_REFRESH));
        }
    }

    public static OrderBy defaultOrder(){
        return OrderBy.fromNameAlias(Flavor_Table.name.getNameAlias()).descending();
    }

    private boolean notNull(){
        return id != -1;
    }

    @Override
    public void delete() {
        super.delete();
        EventBus.getDefault().post(new FlavorEvent(FlavorEvent.TYPE_REFRESH));
    }
}
