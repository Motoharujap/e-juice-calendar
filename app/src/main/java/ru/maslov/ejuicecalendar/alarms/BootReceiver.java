package ru.maslov.ejuicecalendar.alarms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

import ru.maslov.ejuicecalendar.data.JuiceDataProvider;
import ru.maslov.ejuicecalendar.database.model.Juice;

/**
 * Created by Администратор on 01.09.2016.
 */
public class BootReceiver extends BroadcastReceiver {
    private final static String ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    @Override
    public void onReceive(Context context, Intent intent) {
        if (ACTION_BOOT_COMPLETED.equals(intent.getAction())){
            ArrayList<Juice> juices = JuiceDataProvider.getJuices();
            for (Juice juice : juices){
                AlarmScheduler.scheduleJuiceReadyNotification(context, juice);
            }
        }
    }
}
