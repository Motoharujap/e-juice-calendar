package ru.maslov.ejuicecalendar.alarms;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import ru.maslov.ejuicecalendar.R;
import ru.maslov.ejuicecalendar.core.EJuiceApplication;
import ru.maslov.ejuicecalendar.database.model.Juice;
import ru.maslov.ejuicecalendar.tools.RemoteLogger;
import ru.maslov.ejuicecalendar.ui.StartActivity;
import ru.maslov.ejuicecalendar.ui.notifications.JuiceReadyNotificationReceiver;
import ru.maslov.ejuicecalendar.ui.notifications.NotificationCreator;

/**
 * Created by Администратор on 31.08.2016.
 */
public class AlarmScheduler {
    private static final String TAG = AlarmScheduler.class.getSimpleName();

    public static void scheduleJuiceReadyNotification(Context context, Juice juice){
        String notificationTitle = context.getString(R.string.juice_ready_notification_title);
        String notificationMessage = String.format(context.getString(R.string.juice_ready_notification_message), juice.getJuiceName());

        Intent startJuiceListActivityIntent = new Intent(context, StartActivity.class);
        startJuiceListActivityIntent.putExtra(StartActivity.KEY_JUICE_ID, juice.getId());
        startJuiceListActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent notificationAction = PendingIntent.getActivity(context,
                (int) juice.getId(),
                startJuiceListActivityIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = NotificationCreator.simpleActionNotification(context, notificationMessage, notificationTitle, notificationAction);

        Intent notificationIntent = new Intent(context, JuiceReadyNotificationReceiver.class);
        notificationIntent.putExtra(JuiceReadyNotificationReceiver.NOTIFICATION, notification);
        notificationIntent.putExtra(JuiceReadyNotificationReceiver.JUICE_ID, juice.getId());

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, (int) juice.getId(), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(juice.getReadyDate());
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        RemoteLogger.appendLog(EJuiceApplication.getInstance(), "Scheduled alarm with time = " + String.valueOf(calendar.getTimeInMillis()));
    }
}
